#version 330

uniform mat4 projectionMatrix;
uniform mat4 modelViewMatrix;
uniform mat4 normalMatrix;

//shadows
uniform mat4 biasDepthMVP;//converts positions from local to shadow map texture space [0,1]

in vec3 in_Position;
in vec4 in_Colour;
in vec2 in_UV;
in vec3 in_Normal;

out vec3 interpolatedPosition;
out vec4 interpolatedColour;
out vec2 interpolatedUV;
out vec3 interpolatedNormal;
out float fog;

//shadows
out vec4 interpolatedShadowCoord;

void main(void){
	interpolatedPosition = vec3(modelViewMatrix * vec4(in_Position, 1));
	interpolatedColour = in_Colour;
	interpolatedUV = in_UV;
	
	interpolatedNormal = mat3(normalMatrix) * in_Normal;
	
	gl_Position = projectionMatrix * vec4(interpolatedPosition, 1);
	interpolatedShadowCoord = biasDepthMVP * vec4(in_Position, 1);
	
	float distance = sqrt(interpolatedPosition.x * interpolatedPosition.x + interpolatedPosition.y * interpolatedPosition.y + interpolatedPosition.z * interpolatedPosition.z);
	float fogStart = 70.0;
	float fogEnd = 100.0;
	fog = 1;
	if(distance > fogEnd)
		fog = 0;
	else if(distance > fogStart)
		fog = pow(1.0 - (distance - fogStart) / (fogEnd - fogStart), 2);
}