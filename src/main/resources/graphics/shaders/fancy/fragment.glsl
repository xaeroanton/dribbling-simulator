#version 330

uniform sampler2D texture0;
uniform sampler2D shadowMap;
uniform bool useTexture;
uniform vec3 directionToSun;
uniform int shadowMapWidth;
uniform int shadowMapHeight;

in vec3 interpolatedPosition;
in vec4 interpolatedColour;
in vec2 interpolatedUV;
in vec3 interpolatedNormal;
in float fog;

//shadows
in vec4 interpolatedShadowCoord;

out vec4 fragColor;

float getVisibility(vec3 normalizedNormal){
	float cosTheta = max(0, dot(normalizedNormal, directionToSun));
	
	float bias = 0.001 * tan(acos(cosTheta));
	bias = clamp(bias, 0, 0.01);
	
	float range = 4.0;//1.0 for 1 pixel
	float shadowPixelWidth = range / shadowMapWidth;
	float shadowPixelHeight = range / shadowMapHeight;
	
	if(interpolatedShadowCoord.x < 0 || interpolatedShadowCoord.y < 0 || interpolatedShadowCoord.x >= 1.0 || interpolatedShadowCoord.y >= 1.0)//not mapped
		return 1.0;
	
	float total = 0;
	for(int i = -1; i <= 1; i++)
		for(int j = -1; j <= 1; j++){
			float noise = noise1(1) / 2.0 + 0.5;
			if(interpolatedShadowCoord.z - bias > texture2D(shadowMap, vec2(interpolatedShadowCoord.x + i * shadowPixelWidth * noise, interpolatedShadowCoord.y + j * shadowPixelHeight * noise)).z)
				total += 1.0;
		}
		
	return 1.0 - total / 9.0;
}

void main(void){
	vec4 lightColour = vec4(1, 1, 1, 1.0);
	//vec4 lightColour = vec4(1, 0.67, 0.27, 1.0);
	
	vec3 toViewer = normalize(-interpolatedPosition);
	vec3 normalizedNormal = normalize(interpolatedNormal);
	vec3 halfway = normalize(directionToSun + toViewer);
	
	vec4 textureColour = texture2D(texture0, interpolatedUV);
	
	vec4 colour;
	if(useTexture)
		colour = interpolatedColour * textureColour;
	else
		colour = interpolatedColour;
	
	colour = pow(colour, vec4(2.0));
	float alpha = colour.a;
	
	float sunVisibility = getVisibility(normalizedNormal);
	float shadow = sunVisibility;
	
	float ambientLight = 0.3;
	float diffuseLight = max(0, dot(normalizedNormal, directionToSun));
	float specularLight = pow(max(0, dot(normalizedNormal, halfway)), 25);
	if(sunVisibility < 1.0)
		specularLight = 0;
	float shade = ambientLight + 0.6 * min(sunVisibility, diffuseLight) + 0.5 * specularLight;
		
	fragColor = shade * lightColour * colour;
	fragColor = pow(fragColor, vec4(0.5));
	fragColor.a = alpha;
	
	if(fog == 0)
		fragColor = vec4(0, 0, 0, 0);
		//fragColor = vec4(1, 0, 0, 1);
	else if(fog != 1)
		fragColor *= fog;
}