#version 330

uniform mat4 projectionMatrix;
uniform mat4 modelViewMatrix;

in vec3 in_Position;

void main(void){
	vec3 pos = vec3(modelViewMatrix * vec4(in_Position, 1));
	
	gl_Position = projectionMatrix * vec4(pos, 1);
}