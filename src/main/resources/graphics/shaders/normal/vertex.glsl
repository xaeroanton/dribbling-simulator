#version 330

uniform mat4 projectionMatrix;
uniform mat4 modelViewMatrix;
uniform mat4 normalMatrix;

in vec3 in_Position;
in vec4 in_Colour;
in vec2 in_UV;
in vec3 in_Normal;

out vec3 interpolatedPosition;
out vec4 interpolatedColour;
out vec2 interpolatedUV;
out vec3 interpolatedNormal;

void main(void){
	interpolatedPosition = vec3(modelViewMatrix * vec4(in_Position, 1));
	interpolatedColour = in_Colour;
	interpolatedUV = in_UV;
	interpolatedNormal = mat3(normalMatrix) * in_Normal;
	
	gl_Position = projectionMatrix * vec4(interpolatedPosition, 1);
}