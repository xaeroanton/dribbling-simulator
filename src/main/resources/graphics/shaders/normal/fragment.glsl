#version 330

uniform sampler2D texture0;
uniform bool useTexture;

in vec3 interpolatedPosition;
in vec4 interpolatedColour;
in vec2 interpolatedUV;
in vec3 interpolatedNormal;

out vec4 fragColor;

void main(void){
	//fragColor = vec4(interpolatedUV, interpolatedUV.x, 1);
	vec4 textureColour = texture(texture0, interpolatedUV);
	if(useTexture)
		fragColor = interpolatedColour * textureColour;
	else
		fragColor = interpolatedColour;
}