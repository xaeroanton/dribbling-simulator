package game;

import java.io.IOException;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;

import game.controls.ControlsHandler;
import game.controls.GameControls;
import game.gui.GuiGameOver;
import game.gui.GuiPause;
import game.gui.stage.GuiGameStage;
import game.object.Ball;
import game.object.HitDirection;
import game.object.Road;
import game.window.GameWindow;
import graphics.exception.GLException;
import graphics.exception.ShaderException;
import graphics.gui.Gui;
import graphics.object.Camera;
import graphics.object.ShadowMapper;
import graphics.object.model.Cube;
import graphics.object.model.Model;
import graphics.scene.RenderScene;
import graphics.shaders.ShaderProgram;
import graphics.texture.FrameBuffer;
import graphics.texture.FrameBufferAA;
import graphics.texture.Texture;
import math.matrix.OrthogonalMatrix;
import math.matrix.PerspectiveMatrix;
import math.vector.Vector3f;
import math.vector.Vector4f;

public class Game {
	private static final Vector3f SPAWN_POINT = new Vector3f(0, Ball.METER * 1.75f, 5);
	private static final float CAMERA_TO_BALL_DISTANCE = 8f;
	private static final Vector3f directionToSun = new Vector3f(-20, 50, -10);

	private boolean isPaused = false;
	public boolean isRunning;
	//private FrameBuffer frameBuffer;
	private FrameBufferAA frameBufferAA;
	private Model frameBufferModel;
	private RenderScene frameBufferScene;
	private GuiGameStage stage;
	private RenderScene scene3D;
	private Camera mainCamera;
	private Camera currentCamera;
	private ShadowMapper sunShadowMapper;
	private GameControls controls;
	public static ShaderProgram fancyShaders;
	private long gameTickStart;
	private long lastTPS;
	@SuppressWarnings("unused")
	private int tps;
	private int tries = 0;
	private boolean lost = false;
	private boolean gameStarted = false;
	//private int ticksOnGround = 0;
	
	private Ball ball;
	//private Ball ball2;
	private Texture basketballTexture;
	private Road road;
	private HitDirection hitDirection;//the object projecting a model of the direction we're going to hit the ball in
	Model test;
	private Vector4f skyColour;
	private long gameStart;
	
	public Game(GuiGameStage stage){
		this.stage = stage;
		controls = new GameControls(this);
		scene3D = new RenderScene();
		frameBufferScene = new RenderScene();
		skyColour = new Vector4f(0.529f, 0.807f, 0.921f, 1f);
	}
	
	public void preInit() throws GLException, IOException, ShaderException{
		/*
		 * Things that require GL context but don't require the game to be loaded
		 * Called before showing the game window
		 */
        sunShadowMapper = new ShadowMapper("sun", 1024, 1024, directionToSun, directionToSun.negative(), new OrthogonalMatrix(-5f, 5f, 5f, -5f, 0.1f, 100f));
        fancyShaders = new ShaderProgram("fancy");

		road = new Road(10f, 0.3f, 0.2f, 0.04f, 16);
		frameBufferAA = new FrameBufferAA(new Texture(stage.getWindow().getWidth(), stage.getWindow().getHeight()), 4);
		frameBufferAA.init();
		frameBufferModel = Gui.createTexturedRect(stage, frameBufferAA.getTexture(), 0, stage.getWindow().getHeight(), stage.getWindow().getWidth(), 0, 1, 1, 1, 1);
		frameBufferModel.addToScene(frameBufferScene, GameWindow.normalShaders, new Vector3f(0, 0, -1f), Vector3f.ZERO, Vector3f.ONES);
		test = new Cube(null, new Vector4f(1,1,1,1), new Vector4f(1,1,1,1));
		
	}
	
	public void load() throws IOException{
		/*
		 * Loaded during the loading screen
		 * Only things that don't require GL context
		 */
		System.out.println("Loading the game...");
        mainCamera = new Camera(new PerspectiveMatrix(80, (float)stage.getWindow().getWidth()/stage.getWindow().getHeight(), 0.1f, 100f));
        basketballTexture = new Texture("basketball.png");
        currentCamera = mainCamera;
        //loading a test game object
		//moveSpeed = -0.1f;
		System.out.println("Loaded!");
	}
	
	public void init() throws IOException, ShaderException, GLException {
		/*
		 * Things that require both GL context and the game being loaded
		 * Called on starting the game
		 */
		long before = System.currentTimeMillis();
		basketballTexture.upload();
		/*
		 * Basketball texture used: http://www.robinwood.com/Catalog/FreeStuff/Textures/TexturePages/BallMaps.html
		 */
		ball = new Ball(basketballTexture, 60, 30);
		hitDirection = new HitDirection(ball);        
		System.out.println(System.currentTimeMillis() - before);
		road.reset();
		//startGame();
	}
	
	public void unload(){
		isRunning = false;
		//unloading all loaded shaders
		sunShadowMapper.unload();
		if(fancyShaders != null)
			fancyShaders.unloadProgram();
		frameBufferAA.destroy();
	}
	
	public void startGame(){
		lost = false;
		tries++;
		road.reset();
		ball.setLinearVelocity(Vector3f.ZERO);
		ball.setAngularVelocity(Vector3f.ZERO);
		ball.setPosition((road.getCurrentPartLeftBorder() + road.getCurrentPartRightBorder()) / 2.0f, 1.8f * Ball.METER, - Road.ACTIVE_CHUNK * road.getChunkLength());
		mainCamera.setPosition(SPAWN_POINT);
		mainCamera.setPosition(mainCamera.getPosition().x, mainCamera.getPosition().y, - (Road.ACTIVE_CHUNK - 1) * road.getChunkLength());
        mainCamera.setRotation(-45, -30 + (float)Math.random(), 0);
        controls.enableMouseControl();
        controls.gameMode = true;
        if(tries > 1)
        	skyColour = new Vector4f((float)(Math.random() * 0.7), (float)(Math.random() * 0.7), (float)(Math.random() * 0.7), 1f);
        gameStart = System.currentTimeMillis();
        gameStarted = true;
	}
	
	public void gameTick(){//20 tps when isRunning == true
		if(isPaused){
			ControlsHandler.serveKeyInputs(stage);
			return;
		}
		tps++;
		if(System.currentTimeMillis() - lastTPS >= 1000){
			//System.out.println("TPS: " + tps);
			tps = 0;
			lastTPS = System.currentTimeMillis();
		}
		gameTickStart = System.nanoTime();
		//saving current states for usage with partial ticks
		sunShadowMapper.saveState();
		mainCamera.saveState();
		ball.saveState();
		//hand.saveState();
		//sphere.saveState();
		
		ControlsHandler.serveKeyInputs(stage);

		//updating game objects
		//hand.update(mainCamera);
		sunShadowMapper.setPosition(new Vector3f(mainCamera.getPosition().x + directionToSun.x, directionToSun.y, mainCamera.getPosition().z + directionToSun.z - 5));
		//sphere.addToRotation(0, 0.03f, 0);
		//sphere.setPosition(0, 2f + (float)Math.sin(sphere.getRotation().y), 0);
		if (controls.gameMode){
			ball.setPosition(ball.getPosition().x, ball.getPosition().y, -road.getChunksPassed() * road.getChunkLength() - (road.getCurrentPartIndex() + 1) * road.getPartLength());
			//moving camera slowly to the ball
			Vector3f newBallToCamera = mainCamera.getPosition().sum(ball.getPosition().negative()).scale(0.95f);
			mainCamera.setPosition(ball.getPosition().x + newBallToCamera.x, SPAWN_POINT.y, ball.getPosition().z + CAMERA_TO_BALL_DISTANCE);
			
			//updating the road
			if(!lost){
				road.update();
				/*if (gameStarted){
					if (ball.getPosition().y - 1.1f <= 0){
						ticksOnGround++;
						if (ticksOnGround > 20)
							lost = true;
					} else
						ticksOnGround = 0;
				}*/
			}
		}
		if(lost && !(stage.getCurrentScreen() instanceof GuiGameOver))
			stage.openGuiScreen(new GuiGameOver(stage, this, System.currentTimeMillis() - gameStart));
	}
	
	public void renderUpdate(){
		if(isPaused)
			return;
		if(gameStarted && controls.gameMode)
			lost = ball.update(-road.getMoveSpeed() * road.getPartLength(), road.getCurrentPartLeftBorder(), road.getCurrentPartRightBorder(), !lost) || lost;
	}
	
	public void renderTick(){//60 fps
		float partialTicks = (System.nanoTime() - gameTickStart) / GuiGameStage.TICK_TIME;
		if(partialTicks > 1.0f)
			partialTicks = 1.0f;
		
        controls.rotateCamera();//rotate camera with mouse input
        mainCamera.updateView(partialTicks);
		sunShadowMapper.updateView(partialTicks);
		
        //adds all necessary game objects to the scene
		//sphere.addToScene(scene3D, fancyShaders, partialTicks);
		if(gameStarted)
			ball.addToScene(scene3D, fancyShaders, partialTicks);
		road.addToScene(scene3D, fancyShaders);
		test.addToScene(scene3D, fancyShaders, directionToSun.normalize().scale(5), new Vector3f(0,0,0), new Vector3f(1,1,1));

		//maps scene shadows
		sunShadowMapper.mapShadows(scene3D);
		
		//add objects that don't have shadows
		hitDirection.addToScene(scene3D, mainCamera, GameWindow.normalShaders, partialTicks);

		//renders the scene
		frameBufferAA.bind();
        GL11.glClearColor(skyColour.x, skyColour.y, skyColour.z, 1f);
		//frameBuffer.bind();
		GL11.glViewport(0, 0, stage.getWindow().getWidth(), stage.getWindow().getHeight());
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
		scene3D.renderScene(currentCamera, null, sunShadowMapper, false);
		scene3D.clear();
		frameBufferAA.blit();
		frameBufferAA.getTexture().bind(0);
		GL30.glGenerateMipmap(GL11.GL_TEXTURE_2D);
		Texture.unbind(0);
		FrameBuffer.unbind();
		frameBufferScene.renderScene(stage.getWindow().guiCamera, null, null, false);
	}
	
	public Camera getCurrentCamera(){
		return currentCamera;
	}
	
	public void setCurrentCamera(Camera c){
		currentCamera = c;
	}
	
	public Camera getMainCamera(){
		return mainCamera;
	}
	
	public Camera getSunShadowMapper(){
		return sunShadowMapper;
	}
	
	public GuiGameStage getStage(){
		return stage;
	}
	
	public Ball getBall(){
		return ball;
	}
	
	public HitDirection getHitDirection(){
		return hitDirection;
	}
	
	public GameControls getControls(){
		return controls;
	}
	
	//key/mouse event redirection to the GameControls class below

	public boolean shouldKeyRepeat(int key, boolean isMouse){
		return controls.shouldKeyRepeat(key, isMouse);
	}
	
	public void onKeyPress(int key, boolean isMouse){
		if(!isMouse)
			controls.onKeyPress(key);
		else
			controls.onMousePress(key);
	}

	public void onKeyDown(int key, boolean isMouse){
		if(!isMouse)
			controls.onKeyDown(key);
		else{
			controls.onMouseDown(key);
		}
	}

	public void onKeyRelease(int key, boolean isMouse){
		if(!isMouse)
			controls.onKeyRelease(key);
		else
			controls.onMouseRelease(key);
	}
	
	public void pause(){
		if(gameStarted){
			stage.openGuiScreen(new GuiPause(stage, this));
			isPaused = true;
			controls.disableMouseControl();
		}
	}
	
	public void unPause(long pauseStart){
		gameStart += System.currentTimeMillis() - pauseStart;
		isPaused = false;
		if(controls.gameMode)
			controls.enableMouseControl();
	}
}
