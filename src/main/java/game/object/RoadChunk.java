package game.object;

import graphics.object.model.Model;
import graphics.object.model.Vertex;
import graphics.scene.RenderScene;
import graphics.shaders.ShaderProgram;
import math.vector.Vector2f;
import math.vector.Vector3f;
import math.vector.Vector4f;

public class RoadChunk {
	
	Model model;
	private RoadPart[] roadParts;
	private static final float PART_THICKNESS = 0.5f;
	private static final float MIN_WIDTH = 2f;
	private float turnSpeed;
	private float change;
	private float lastNX;
	private float lastNZ;
	private Road road;
	
	//Vector4f colour = new Vector4f((float)Math.random(), (float)Math.random(), (float)Math.random(), 1f);
	Vector4f colour = new Vector4f(1f, 1f, 1f, 1f);
	
	public RoadChunk(Road road, float lastEnd1, float lastEnd2,
			float lastNX, float lastNZ, float turnSpeed, float change, boolean straight){
		this.road = road;
		this.roadParts = new RoadPart[road.getChunkSize()];
		this.turnSpeed = turnSpeed;
		this.change = change/road.getChunkSize();
		
		
		model = new Model();
		
		//first part init
		roadParts[0] = new RoadPart(lastEnd1, lastEnd2);
		addVerticesToModel(lastEnd1, lastEnd2, lastNX, lastNZ, 0);
		addRoadParts(lastEnd1, lastEnd2, straight);
	}

	public RoadPart getLastPart(){
		return roadParts[roadParts.length - 1];
	}
	
	public float getTurnSpeed(){
		return turnSpeed;
	}
	
	public float getLastNX(){
		return lastNX;
	}
	
	public float getLastNZ(){
		return lastNZ;
	}
	
	public RoadPart getPartByIndex(int index){
		return roadParts[index];
	}
	
	public void addRoadParts(float lastEnd1, float lastEnd2, boolean straight){
		//add new randomly turning road parts
		float nX = 1;
		float nZ = 0;
		
		for (int i = 0; i < roadParts.length; i++){
			float newEnd1 = lastEnd1+turnSpeed-change;
			float newEnd2 = lastEnd2+turnSpeed+change;
			
			//check part width
			float partWidth = Math.abs(newEnd1 - newEnd2);
			if (partWidth < MIN_WIDTH){
				float middlePoint = newEnd1 + partWidth/2;
				newEnd1 = middlePoint - MIN_WIDTH/2;
				newEnd2 = middlePoint + MIN_WIDTH/2;
			}
			roadParts[i] = new RoadPart(newEnd1, newEnd2);
			
//			//count normals
			float offX = turnSpeed;
			float offZ = road.getPartLength();
			float offLength = (float) Math.sqrt(offX * offX + offZ * offZ);
			nX = -offZ / offLength;
			nZ = -offX / offLength;
			
			addToModel(newEnd1, newEnd2, nX, nZ, -(i+1)*road.getPartLength());
			
			if (!straight) {
				turnSpeed += -road.getMaxTurnAcceleration() + road.getMaxTurnAcceleration()*2 * (float) Math.random();
				if (Math.abs(turnSpeed) > road.getMaxTurnSpeed())
					turnSpeed = Math.copySign(road.getMaxTurnSpeed(), turnSpeed);
			}
			
			lastEnd1 = roadParts[i].getEnd1();
			lastEnd2 = roadParts[i].getEnd2();
		}
		
		this.lastNX = nX;
		this.lastNZ = nZ;
	}
		
	public void addToModel(float end1, float end2, float nX, float nZ, float posZ){
		
		addVerticesToModel(end1, end2, nX, nZ, posZ);
		
		int modelSize = model.getVerticesCount();
		
		//current vertices
		int topLeft = modelSize - 6;
		int topRight = modelSize - 5;
		int leftBot = modelSize - 4;
		int leftTop = modelSize - 3;
		int rightTop = modelSize - 2;
		int rightBot = modelSize - 1;
		
		//lastPart vertices
		int lastTopLeft = modelSize - 12;
		int lastTopRight = modelSize - 11;
		int lastLeftBot = modelSize - 10;
		int lastLeftTop = modelSize - 9;
		int lastRightTop = modelSize - 8;
		int lastRightBot = modelSize - 7;
		
		//top
		model.addTriangle(lastTopLeft, lastTopRight, topLeft);
		model.addTriangle(topLeft, lastTopRight, topRight);
		
		//left
		model.addTriangle(lastLeftBot, lastLeftTop, leftBot);
		model.addTriangle(leftBot, lastLeftTop, leftTop);
		
		//right
		model.addTriangle(lastRightTop, lastRightBot, rightTop);
		model.addTriangle(rightTop, lastRightBot, rightBot);
	}
	
	public void addVerticesToModel(float end1, float end2, float nX, float nZ, float posZ){
		model.addVertices(new Vertex[] {
				//top vertices
				new Vertex(end1, 0, posZ, colour, new Vector2f(1, 0), new Vector3f(0, 1, 0)), //size-6
				new Vertex(end2, 0, posZ, colour, new Vector2f(1, 1), new Vector3f(0, 1, 0)), //size-5
//				//left face vertices
				new Vertex(end1, -PART_THICKNESS, posZ, colour, new Vector2f(1, 0), new Vector3f(nX, 0, nZ)), //size-4 
				new Vertex(end1, 0, posZ, colour, new Vector2f(1, 0), new Vector3f(nX, 0, nZ)), //size-3
//				//right face vertices
				new Vertex(end2, 0, posZ, colour, new Vector2f(1, 1), new Vector3f(-nX, 0, -nZ)), //size-2
				new Vertex(end2, -PART_THICKNESS, posZ, colour, new Vector2f(1, 1), new Vector3f(-nX, 0, -nZ)) //size-1
		});
	}
	
	public void addToScene(RenderScene scene, ShaderProgram shaders, float originX, float originY, float originZ){
		model.addToScene(scene, shaders, new Vector3f(originX, originY, originZ), new Vector3f(0,0,0), new Vector3f(1,1,1));
	}
}
