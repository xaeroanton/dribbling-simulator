package game.object;

import graphics.object.model.Model;
import graphics.object.model.Sphere;
import graphics.scene.RenderScene;
import graphics.shaders.ShaderProgram;
import graphics.texture.Texture;
import math.object.SmoothSpaceObject;

public class CenterSphere extends SmoothSpaceObject {
	
	//this is a test game object that has a model
	
	private Model model;
	
	public CenterSphere(Texture texture){
		super();
		model = new Sphere(texture, 40, 20);
		//model = new Cube(texture, new Vector4f(0.5f, 0.5f, 0.5f, 0.5f), new Vector4f(1f, 1f, 1f, 1f));
	}
	
	public void addToScene(RenderScene scene, ShaderProgram shaders, float partialTicks){
		model.addToScene(scene, shaders, getPosition(partialTicks), getRotation(partialTicks), getScale(partialTicks));
	}
	
	public Model getModel(){
		return model;
	}
	
}
