package game.object;

import graphics.object.Camera;
import graphics.object.model.Cilinder;
import graphics.object.model.Model;
import graphics.scene.RenderScene;
import graphics.shaders.ShaderProgram;
import math.vector.Vector3f;
import math.vector.Vector4f;

public class HitDirection {
	public static final float MIN_HIT_DISTANCE = 10f;
	private static final float ALPHA = 0.3f;
	private static final float MODEL1_LENGTH = 1.0f;
	private static final float MODEL2_LENGTH = 0.1f;
	private static final float MODEL3_LENGTH = 1.0f;
	private Vector3f cursorPos;
	private Model model1;
	private Model model2;
	private Model model3;
	private Ball ball;
	public byte hitState;

	public HitDirection(Ball ball){
		super();
		this.hitState = 0;
		this.ball = ball;
		Vector4f colour = new Vector4f(1f * ALPHA, 0f * ALPHA, 0f * ALPHA, ALPHA);
		model1 = new Cilinder(null, 40, colour, new Vector4f(0f, 0f, 0f, 0f), false, false);
		model2 = new Cilinder(null, 40, colour, colour, false, false);
		model3 = new Cilinder(null, 40, new Vector4f(0f, 0f, 0f, 0f), colour, false, false);
	}

	public void addToScene(RenderScene scene, Camera camera, ShaderProgram shaders, float partialTicks){
		cursorPos = getCursorPos(camera, partialTicks);
		Vector3f cursorToBall = ball.getPosition().sum(cursorPos.negative());
		float distance = cursorToBall.length();
		if(distance <= MIN_HIT_DISTANCE){
			Vector3f modelToBall = getVectorModelToBall(cursorToBall);
			Vector3f modelPos = ball.getPosition().sum(modelToBall);
			modelPos.z = ball.getPosition(partialTicks).z;
			Vector3f modelRot = new Vector3f(0, 0, -getModelRotation(modelToBall.negative()));
			Vector3f modelScale = new Vector3f(1, MODEL1_LENGTH, 1);
			model1.addToScene(scene, shaders, modelPos, modelRot, modelScale);
			modelPos = modelPos.sum(modelToBall.normalize().scale((MODEL1_LENGTH + MODEL2_LENGTH) / 2.0f));
			modelScale.y = MODEL2_LENGTH;
			model2.addToScene(scene, shaders, modelPos, modelRot, modelScale);
			modelPos = modelPos.sum(modelToBall.normalize().scale((MODEL2_LENGTH + MODEL3_LENGTH) / 2.0f));
			modelScale.y = MODEL3_LENGTH;
			model3.addToScene(scene, shaders, modelPos, modelRot, modelScale);
		}
	}

	private Vector3f getCursorPos(Camera camera, float partialTicks){
		Vector3f camPos = camera.getPosition(partialTicks);
		Vector3f camLookVector = camera.getLookVector();
		if(camLookVector.z == 0)
			camLookVector.z = 0.0001f;
		float distanceZ = (ball.getPosition(partialTicks).z - camPos.z) / camLookVector.z;
		Vector3f pos = new Vector3f(camPos.x + camLookVector.x * distanceZ, camPos.y + camLookVector.y * distanceZ, ball.getPosition().z);
		return pos;
	}

	public Vector3f getVectorModelToBall(Vector3f cursorToBall){
		Vector3f modelToBall = new Vector3f(cursorToBall.x, cursorToBall.y, 0);
		float distanceToBall = modelToBall.length();
		if(distanceToBall == 0){
			cursorToBall.x = 0;
			cursorToBall.y = 1f + MODEL1_LENGTH / 2.0f;
		} else /*if(distanceToBall - MODEL_LENGTH / 2.0f < 1f)*/
			modelToBall = cursorToBall.scale((1f + MODEL1_LENGTH / 2.0f)/distanceToBall);
		if(modelToBall.y > 0)
			modelToBall = new Vector3f(Math.copySign(1f + MODEL1_LENGTH / 2.0f, modelToBall.x), 0, 0);
		return modelToBall;
	}

	private float getModelRotation(Vector3f fromBall){
		float distance = fromBall.length();
		float rotation = (float) (Math.asin(-fromBall.x / distance) * 180/ Math.PI);
		if (fromBall.y > 0)
			rotation = 180 * ((fromBall.x < 0) ? -1 : 1) - rotation;
		return rotation;
	}
	
	public Vector3f getCursorPos(){
		return cursorPos;
	}

}
