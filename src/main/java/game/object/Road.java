package game.object;

import java.util.ArrayList;

import graphics.scene.RenderScene;
import graphics.shaders.ShaderProgram;

public class Road {
	public static final int ACTIVE_CHUNK = 22;
	public static final int ROAD_SIZE = 41;
	private ArrayList<RoadChunk> roadChunks;
	private float roadWidth;
	private float partLength;
	private float maxTurnAcceleration;
	private float maxTurnSpeed;
	private int chunkSize;
	private int moveSpeed;
	private int currentPartIndex;
	private RoadPart currentPart;
	private float roadStartPos;
	private int chunksPassed;
	
	public Road(float roadWidth, float partLength, float maxTurnSpeed, float maxTurnAcceleration, int chunkSize){
		this.roadWidth = roadWidth;
		this.partLength = partLength;
		this.maxTurnSpeed = maxTurnSpeed;
		this.maxTurnAcceleration = maxTurnAcceleration;
		this.chunkSize = chunkSize;
		roadChunks = new ArrayList<>();
	}
	
	public void reset(){
		this.currentPartIndex = 0;
		this.roadStartPos = 0;
		this.moveSpeed = 1;
		this.chunksPassed = ACTIVE_CHUNK;
		while(!roadChunks.isEmpty())
			roadChunks.remove(0).model.destroyModel();
		roadChunks.add(new RoadChunk(this, -roadWidth/2, roadWidth/2, 1, 0, 0, 0, true));
		for (int i = 0; i < ACTIVE_CHUNK; i++)
			addRoadChunk(0, true);
		for (int i = ACTIVE_CHUNK; i < ROAD_SIZE; i++)
			addRoadChunk(0, false);
		this.currentPart = roadChunks.get(ACTIVE_CHUNK).getPartByIndex(currentPartIndex);
	}
	
	public void addToScene(RenderScene scene, ShaderProgram shaders){
		for(int i = 0; i < roadChunks.size(); i++)
			roadChunks.get(i).addToScene(scene, shaders, 0, 0, -partLength*chunkSize*i + roadStartPos);
	}
	
	public void addRoadChunk(float change, boolean straight){
		RoadChunk lastChunk = roadChunks.get(roadChunks.size()-1);
		RoadPart lastPart = lastChunk.getLastPart();
		roadChunks.add(new RoadChunk(this, lastPart.getEnd1(), lastPart.getEnd2(),
				lastChunk.getLastNX(), lastChunk.getLastNZ(), lastChunk.getTurnSpeed(), change, straight));
	}
	
	public float getChunkLength(){
		return chunkSize * partLength;
	}
	
	public void update(){ // we are always on the second roadChunk
		currentPartIndex += moveSpeed;
		if (currentPartIndex >= chunkSize){
			int chunkDelta = currentPartIndex / chunkSize;
			chunksPassed += chunkDelta;
			currentPartIndex = currentPartIndex % chunkSize;
			for (int i = 0; i < chunkDelta; i++){
				addRoadChunk(0, false);
				roadChunks.remove(0).model.destroyModel();
			}
			roadStartPos -= getChunkLength() * chunkDelta;
		}
		currentPart = roadChunks.get(ACTIVE_CHUNK).getPartByIndex(currentPartIndex);
		
	}
	
	public void setMaxTurnAcceleration(float a){
		maxTurnAcceleration = a;
	}
	
	public void increaseMaxTurnAcceleration(float percent){
		maxTurnAcceleration = maxTurnAcceleration * (percent/100f + 1);
	}

	public float getPartLength() {
		return partLength;
	}

	public float getMaxTurnAcceleration() {
		return maxTurnAcceleration;
	}

	public float getMaxTurnSpeed() {
		return maxTurnSpeed;
	}

	public int getChunkSize() {
		return chunkSize;
	}

	public float getMoveSpeed() {
		return moveSpeed;
	}

	public void setMoveSpeed(int moveSpeed) {
		this.moveSpeed = moveSpeed;
	}
	
	public int getCurrentPartIndex(){
		return currentPartIndex;
	}
	
	public float getCurrentPartLeftBorder(){
		return currentPart.getEnd1();
	}
	
	public float getCurrentPartRightBorder(){
		return currentPart.getEnd2();
	}
	
	public int getChunksPassed(){
		return chunksPassed;
	}
}
