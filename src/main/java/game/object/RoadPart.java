package game.object;

public class RoadPart{
	private float end1;
	private float end2;
	
	public RoadPart(float end1, float end2){
		this.end1 = end1;
		this.end2 = end2;
	}
	
	public float getEnd1(){
		return end1;
	}
	
	public float getEnd2(){
		return end2;
	}
	
}
