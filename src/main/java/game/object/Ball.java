package game.object;

import game.Game;
import game.gui.stage.GuiGameStage;
import graphics.object.model.BallModel;
import graphics.scene.RenderScene;
import graphics.shaders.ShaderProgram;
import graphics.texture.Texture;
import math.object.SpacePhysicalObject;
import math.vector.Vector3f;

public class Ball extends SpacePhysicalObject {

	/*
	 * Standart Basketball parameters
	 */
	private static final float BALL_RADIUS = 0.12f;//m
	private static final float CIRCUMFERENCE = (float) (2.0 * Math.PI * BALL_RADIUS);
	private static final float DRAG_COEFFICIENT = 0.47f;//drag coefficient of a sphere
	private static final float CROSSSECTIONAL_AREA = (float) (Math.PI * BALL_RADIUS * BALL_RADIUS);//m2
	private static final float AIR_PRESSURE = 500000f;//dropped from 1.8 metres would come back 2/3 if that - 1.2 metres
	

	private static final float AIR_DENSITY = 1.225f;//kg/m3
	public static final float METER = 1.0f / BALL_RADIUS;//units

	private BallModel model;
	private float airFrictionCoefficient;
	
	public Ball(Texture texture, int yawSectors, int pitchSectors){
		super();
		model = new BallModel(texture, yawSectors, pitchSectors);
		/*
		 * The air friction formula used: http://hyperphysics.phy-astr.gsu.edu/hbase/airfri.html
		 */
		airFrictionCoefficient = 0.5f * DRAG_COEFFICIENT * AIR_DENSITY * CROSSSECTIONAL_AREA;
	}
	
	public void addToScene(RenderScene scene, ShaderProgram shaders, float partialTicks){
		model.addToScene(scene, shaders, new Vector3f(position.x, position.y, getPosition(partialTicks).z), scale, rotation, Vector3f.ONES);
	}

	int RUNS = 30;
	boolean hit = false;
	//private Vector3f acceleration = new Vector3f(0,0,0);
	public boolean update(float zSpeed, float leftEnd, float rightEnd, boolean movingForward) {//returns whether the ball fell off the road
		float tps = 1000000000.0f / GuiGameStage.TICK_TIME;
		float timeInSeconds = GuiGameStage.FRAME_LENGTH / 1000000000.0f / RUNS;
		float zSpeedPerSecond = zSpeed * tps;
		float linearZSpeed = movingForward ? linearToAngular(zSpeedPerSecond) : 0;
		Vector3f gravity = new Vector3f(0, (-9.8f * METER) * timeInSeconds, 0);//would add -9.8 meters/s every 1-second tick
		for(int i = 0; i < RUNS; i++){
			
			Vector3f linearAcceleration = new Vector3f(0,0,0);
			Vector3f angularAcceleration = new Vector3f(0,0,0);
			linearAcceleration = linearAcceleration.sum(gravity);
			linearAcceleration = linearAcceleration.sum(getAirFrictionForce(getLinearVelocity()).scale(timeInSeconds));
			if(position.y < 1.0){
				float boxX = (float) Math.sqrt(1 - position.y * position.y);//within what local coordinates can the ball collide with the road (circle X2 + Y2 = R2)
				if(position.x + boxX >= leftEnd && position.x - boxX <= rightEnd){
					//calculating the boundaries touched by the road
					float collisionLeft = Math.max(position.x - boxX, leftEnd);
					float collisionRight = Math.min(position.x + boxX, rightEnd);
					float collisionCenter = (collisionLeft + collisionRight)/2.0f;
					Vector3f fromCollisionCenter = new Vector3f(position.x - collisionCenter, position.y /*- roadHeight*/, 0);
					float compression = 1.0f - fromCollisionCenter.length();
					if(getLinearVelocity().y < 0){
						setLinearVelocity(getLinearVelocity().x, getLinearVelocity().y * 0.9f, getLinearVelocity().z);//ground stealing a lot of energy from the ball
						setAngularVelocity(getAngularVelocity().scale(0.9f));
						hit = true;
					}
					Vector3f forceDirection = fromCollisionCenter.normalize();
					linearAcceleration = linearAcceleration.sum(forceDirection.scale(AIR_PRESSURE * (float)compression * timeInSeconds));
					setAngularVelocity(new Vector3f(linearZSpeed, 0, linearToAngular(forceDirection.y * -getLinearVelocity().x + forceDirection.x * getLinearVelocity().y)));
					//if(Math.abs(collisionCenter-position.x) > 0.4f)//if colliding with a side of the road, != doesn't work because of float precision
						//addAngularVelocity(0, 0, linearToAngular((collisionCenter > position.x ? -1 : 1) * getLinearVelocity().y));
				}
			} else {
				hit = false;
				//angularAcceleration = angularAcceleration.sum(getAirFrictionForce(getAngularVelocity()).scale(timeInSeconds));
			}
			addLinearVelocity(linearAcceleration);
			addAngularVelocity(angularAcceleration);
			
			addToPosition(linearVelocity.scale(timeInSeconds));
			model.rotate(angularVelocity, timeInSeconds);
		}
		return position.y + 1.0f < 0f && (position.x < leftEnd || position.x > rightEnd);
	}
	
	public void update(float time){
	}
	
	private float linearToAngular(float linear){
		return linear / (CIRCUMFERENCE * METER) * 360.0f;
	}

	private Vector3f getAirFrictionForce(Vector3f linearVelocity) {
		return linearVelocity.normalize().negative().scale((float)Math.pow(linearVelocity.length() / METER, 2) * airFrictionCoefficient * METER);
	}
	
	public void forceMovement(Game game){//"throwing" the ball
		if(position.y <= 1.0f)//to stop cheating
			return;
		Vector3f cursorPos = game.getHitDirection().getCursorPos();
		Vector3f directionToBall = game.getHitDirection().getVectorModelToBall(getPosition().sum(cursorPos.negative())).normalize();
		float distance = directionToBall.length();
		if(distance <= HitDirection.MIN_HIT_DISTANCE){
			addLinearVelocity(directionToBall.scale(5));
			setAngularVelocity(getAngularVelocity().scale(0.5f));
		}
	}
}
