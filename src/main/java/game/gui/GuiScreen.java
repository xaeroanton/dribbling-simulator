package game.gui;

import game.gui.stage.GuiStage;
import game.window.GameWindow;
import graphics.gui.Gui;
import graphics.object.model.Model;
import graphics.object.model.ModelString;
import graphics.texture.Texture;
import math.vector.Vector3f;
import math.vector.Vector4f;

public abstract class GuiScreen {
	
	protected GuiStage stage;
	
	public GuiScreen(GuiStage stage){
		this.stage = stage;
	}
	
	protected void addToGuiScene(Model model){
		addToGuiScene(model, Vector3f.ZERO, Vector3f.ZERO, Vector3f.ONES);
	}
	
	protected void addToGuiScene(Model model, Vector3f position, Vector3f rotation, Vector3f scale){
		model.addToScene(stage.getGUIScene(), GameWindow.normalShaders, position, rotation, scale);
	}
	
	protected void addCenteredStringToGuiScene(ModelString model, Vector3f position, boolean centerY){
		model.addCenteredToScene(stage.getGUIScene(), GameWindow.normalShaders, position, Vector3f.ZERO, Vector3f.ONES, centerY);
	}
	
	protected void addCenteredStringToGuiScene(ModelString model, Vector3f position, Vector3f rotation, Vector3f scale, boolean centerY){
		model.addCenteredToScene(stage.getGUIScene(), GameWindow.normalShaders, position, rotation, scale, centerY);
	}
	
	protected ModelString createString(String text, Vector4f colour){
		return stage.getWindow().fontRenderer.createString(text, colour);
	}
	
	protected Model createRect(int left, int top, int right, int bottom, float r, float g, float b, float a){
		return Gui.createRect(stage, left, top, right, bottom, r, g, b, a);
	}
	
	protected Model createTexturedRect(Texture texture, int left, int top, int right, int bottom, float r, float g, float b, float a){
		return Gui.createTexturedRect(stage, texture, left, top, right, bottom, r, g, b, a);
	}
	
	public abstract void init();
	public abstract void drawScreen();

	public abstract void onKeyPress(int key, boolean isMouse);
	public abstract void onKeyDown(int key, boolean isMouse);
	public abstract void onKeyRelease(int key, boolean isMouse);
	
}
