package game.gui;

import org.lwjgl.glfw.GLFW;

import game.gui.stage.GuiGameStage;
import game.gui.stage.GuiStage;
import graphics.object.model.ModelString;
import math.vector.Vector3f;
import math.vector.Vector4f;

public class GuiMainMenu extends GuiScreen {
	
	private ModelString mainMenuString;
	private ModelString infoString;
	
	public GuiMainMenu(GuiStage stage){
		super(stage);
	}

	@Override
	public void init() {
		mainMenuString = createString("DRIBBLING SIMULATOR", new Vector4f(1, 1, 1, 1));
		infoString = createString("Press ENTER to start", new Vector4f(1, 1, 1, 1));
	}

	@Override
	public void drawScreen() {
		addCenteredStringToGuiScene(mainMenuString, new Vector3f(stage.getWindow().getWidth() / 2, stage.getWindow().getHeight(), 0), Vector3f.ZERO, new Vector3f(0.5f,0.5f,0.5f), false);
		addCenteredStringToGuiScene(infoString, new Vector3f(stage.getWindow().getWidth() / 2, stage.getWindow().getHeight() / 2, 0), Vector3f.ZERO, new Vector3f(0.25f,0.25f,1f), false);
	}

	@Override
	public void onKeyPress(int key, boolean isMouse) {
		if(!isMouse){
			if(key == GLFW.GLFW_KEY_ENTER){
				stage.openGuiScreen(null);
				((GuiGameStage)stage).getGame().startGame();
				//((GameWindow)stage.getWindow()).showStage(((GameWindow)stage.getWindow()).gameStage);
			} else if(key == GLFW.GLFW_KEY_ESCAPE)
				stage.getWindow().shouldClose = true;
		}
	}

	@Override
	public void onKeyDown(int key, boolean isMouse) {
	}

	@Override
	public void onKeyRelease(int key, boolean isMouse) {
	}

}
