package game.gui;

import game.gui.stage.GuiStage;
import graphics.object.model.ModelString;
import math.vector.Vector3f;
import math.vector.Vector4f;

public abstract class GuiLoading extends GuiScreen {
	
	private ModelString loadingString;

	public GuiLoading(GuiStage stage) {
		super(stage);
	}

	@Override
	public void init(){
		loadingString = createString("Loading...", new Vector4f(1, 1, 1, 1));
	}

	@Override
	public void drawScreen() {
		addCenteredStringToGuiScene(loadingString, new Vector3f(stage.getWindow().getWidth() / 2, stage.getWindow().getHeight() / 2, 0), Vector3f.ZERO, new Vector3f(0.25f,0.25f,1f), true);
	}

	@Override
	public void onKeyPress(int key, boolean isMouse) {
	}

	@Override
	public void onKeyDown(int key, boolean isMouse) {
	}

	@Override
	public void onKeyRelease(int key, boolean isMouse) {
	}

}
