package game.gui;

import org.lwjgl.glfw.GLFW;

import game.Game;
import game.gui.stage.GuiStage;
import graphics.object.model.Model;
import graphics.object.model.ModelString;
import math.vector.Vector3f;
import math.vector.Vector4f;

public class GuiPause extends GuiScreen {
	
	private Game game;
	private Model background;
	private ModelString pauseString;
	private long pauseStart;

	public GuiPause(GuiStage stage, Game game) {
		super(stage);
		this.game = game;
	}

	@Override
	public void init() {
		background = createRect(0, stage.getWindow().getHeight(), stage.getWindow().getWidth(), 0, 0.3f, 0.3f, 0.3f, 0.5f);
		pauseString = createString("PAUSE", new Vector4f(1, 1, 1, 1));
		pauseStart = System.currentTimeMillis();
	}

	@Override
	public void drawScreen() {
		addToGuiScene(background, new Vector3f(0, 0, -0.1f), Vector3f.ZERO, Vector3f.ONES);
		addCenteredStringToGuiScene(pauseString, new Vector3f(stage.getWindow().getWidth() / 2, stage.getWindow().getHeight() / 2, 0), Vector3f.ZERO, new Vector3f(0.5f,0.5f,1f), true);
	}

	@Override
	public void onKeyPress(int key, boolean isMouse) {
		if(!isMouse){
			if(key == GLFW.GLFW_KEY_ESCAPE){
				stage.openGuiScreen(null);
				game.unPause(pauseStart);
			}
		}
	}

	@Override
	public void onKeyDown(int key, boolean isMouse) {
	}

	@Override
	public void onKeyRelease(int key, boolean isMouse) {
	}

}
