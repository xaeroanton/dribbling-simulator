package game.gui;

import org.lwjgl.glfw.GLFW;

import game.Game;
import game.gui.stage.GuiStage;
import graphics.object.model.Model;
import graphics.object.model.ModelString;
import math.vector.Vector3f;
import math.vector.Vector4f;

public class GuiGameOver extends GuiScreen{

	private Game game;
	private Model background;
	private ModelString gameOverString;
	private ModelString timeString;
	private ModelString infoString;
	private float time;
	private int stringTimer = 0;

	public GuiGameOver(GuiStage stage, Game game, long time) {
		super(stage);
		this.game = game;
		this.time = Math.round(time / 1000.0f * 100f) / 100f;
	}

	@Override
	public void init() {
		float alpha = 0.6f;
		background = createRect(0, stage.getWindow().getHeight(), stage.getWindow().getWidth(), 0, 0.784f * alpha, 0f * alpha, 0f * alpha, alpha);
		gameOverString = createString("GAME OVER", new Vector4f(1, 1, 1, 1));
		timeString = createString("Time: " + time + "s", new Vector4f(1, 1, 1, 1));
		infoString = createString("R - Restart   ESC - Exit", new Vector4f(1, 1, 1, 1));
	}

	@Override
	public void drawScreen() {
		stringTimer++;
		addToGuiScene(background, new Vector3f(0, 0, -0.1f), Vector3f.ZERO, Vector3f.ONES);
		if(stringTimer > 35){
			addCenteredStringToGuiScene(gameOverString, new Vector3f(stage.getWindow().getWidth() / 2, stage.getWindow().getHeight() / 2, 0), Vector3f.ZERO, new Vector3f(0.5f,0.5f,1f), true);
			addCenteredStringToGuiScene(timeString, new Vector3f(stage.getWindow().getWidth() / 2, stage.getWindow().getHeight() / 2 - 50, 0), Vector3f.ZERO, new Vector3f(0.25f,0.25f,1f), true);
		}
		if(stringTimer > 70)
			addCenteredStringToGuiScene(infoString, new Vector3f(stage.getWindow().getWidth() / 2, 35, 0), Vector3f.ZERO, new Vector3f(0.2f,0.2f,1f), true);
	}

	@Override
	public void onKeyPress(int key, boolean isMouse) {
		if(!isMouse){
			if(key == GLFW.GLFW_KEY_R){
				game.startGame();
				stage.openGuiScreen(null);
				if(game.getControls().gameMode)
					game.getControls().enableMouseControl();
			}
			if(key == GLFW.GLFW_KEY_ESCAPE)
				game.getStage().getWindow().shouldClose = true;
		}
	}

	@Override
	public void onKeyDown(int key, boolean isMouse) {
	}

	@Override
	public void onKeyRelease(int key, boolean isMouse) {
	}

}