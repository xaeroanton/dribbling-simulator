package game.gui.stage;

import game.gui.GuiScreen;
import graphics.scene.RenderScene;
import graphics.window.Window;

/*
 * GuiStage is literally a stage for displaying GuiScreens
 * For example: GuiGameStage renders a 3D world which becomes the stage for the ingame GUI screens that are rendered on top (pause screen etc).
 * If there is no currentScreen on the stage then all we see is the stage itself.
 */

public abstract class GuiStage {

	protected Window window;
	protected GuiScreen newScreen = null;
	protected GuiScreen currentScreen = null;
	protected RenderScene guiScene;
	
	public GuiStage(Window window){
		this.window = window;
		guiScene = new RenderScene();
	}
	
	public void drawStage(){
		if(newScreen != currentScreen){
			if(newScreen != null)
				newScreen.init();
			currentScreen = newScreen;
		}
		if(currentScreen != null)
			currentScreen.drawScreen();
		guiScene.renderScene(window.guiCamera, null, null, false);
		guiScene.clear();
	}

	public abstract void preInit();//called before showing the window
	public abstract void init();//called on switching to the stage
	public abstract void closeStage();

	public abstract void onKeyPress(int key, boolean isMouse);
	public abstract void onKeyDown(int key, boolean isMouse);
	public abstract void onKeyRelease(int key, boolean isMouse);

	public boolean shouldKeyRepeat(int key, boolean isMouse) {
		return true;
	}
	
	public Window getWindow(){
		return window;
	}
	
	public RenderScene getGUIScene(){
		return guiScene;
	}
	
	public void openGuiScreen(GuiScreen screen){
		newScreen = screen;
	}
	
	public GuiScreen getCurrentScreen(){
		return currentScreen;
	}
	
}
