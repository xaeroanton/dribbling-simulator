package game.gui.stage;

import java.io.IOException;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import game.Game;
import game.window.GameWindow;
import graphics.exception.GLException;
import graphics.exception.ShaderException;

/*
 * For showing GUI screens on-top of the game
 */

public class GuiGameStage extends GuiStage {
	
	public static final int WANTED_FPS = 60;
	public static final float FRAME_LENGTH = 1000000000.0f/WANTED_FPS;
	public static final int FRAMES_PER_GAME_TICK = 3;
	public static final float TICK_TIME = FRAME_LENGTH * FRAMES_PER_GAME_TICK;
	public static final float TPS = 1000000000.0f / TICK_TIME;
	private static long lastGameTick = 0;
	private static long lastRenderUpdate = 0;
	private Game game;
	
	public GuiGameStage(GameWindow gameWindow) {
		super(gameWindow);
        game = new Game(this);
	}
	
	public Game getGame(){
		return game;
	}
	
	@Override 
	public void preInit(){
		try {
			game.preInit();
		} catch (GLException | IOException | ShaderException e) {
			e.printStackTrace(System.err);
			System.exit(-1);
		}
	}

	@Override
	public void init(){
		try {
			game.init();
		} catch (Exception e) {
			e.printStackTrace(System.err);
			System.exit(-1);
		}
	}
	
	@Override
	public void drawStage(){
		/*
		 * Game ticks
		 */
		long drawStart = System.nanoTime();
		int[] gameTickCount = getTickAmountAndLag(drawStart, lastGameTick, FRAMES_PER_GAME_TICK);
		int gameTicksNeeded = gameTickCount[0];
		int lag = gameTickCount[1];//can't lose precious time
		if(gameTicksNeeded > 0){
			lastGameTick = drawStart - lag;
			for(int i = 0; i < gameTicksNeeded; i++){
				game.gameTick();
				if(i > 30)//LAG
					break;
			}
		}
		/*
		 * Render update ticks (for objects that are updated every frame)
		 */
		int[] renderUpdateCount = getTickAmountAndLag(drawStart, lastRenderUpdate, 1);
		int renderUpdatesNeeded = renderUpdateCount[0];
		lag = renderUpdateCount[1];
		if(renderUpdatesNeeded > 0){
			lastRenderUpdate = drawStart - lag;
			for(int i = 0; i < renderUpdatesNeeded; i++){
				game.renderUpdate();
				if(i > 30 * FRAMES_PER_GAME_TICK)//LAG
					break;
			}
		}
		/*
		 * Actual render tick
		 */
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_ONE, GL11.GL_ONE_MINUS_SRC_ALPHA);
        game.renderTick();
		GL20.glUseProgram(0);
    	GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
		GL20.glDisableVertexAttribArray(0);//position
		GL20.glDisableVertexAttribArray(1);//colour
		GL20.glDisableVertexAttribArray(2);//uv
		GL20.glDisableVertexAttribArray(3);//normal
    	GL30.glBindVertexArray(0);
		super.drawStage();
	}
	
	private static int[] getTickAmountAndLag(long currentTime, long lastTick, float framesPerTick){
		long timePassed = currentTime - lastTick;
		int framesPassed = (int) (timePassed / FRAME_LENGTH);
		int lag = (int) (timePassed - framesPassed * FRAME_LENGTH);//can't lose precious time
		int ticksNeeded = lastTick == 0 ? 1 : (int) (framesPassed / framesPerTick);
		return new int[]{ticksNeeded, lag};
	}
	
	@Override 
	public void closeStage(){
		if(game.isRunning)
			game.unload();
	}

	@Override
	public void onKeyPress(int key, boolean isMouse) {
		if(currentScreen == null)//currentScreen == null means that we are ingame
			game.onKeyPress(key, isMouse);
		else //otherwise we are most likely in the pause menu or any other screen that has the game in the background
			currentScreen.onKeyPress(key, isMouse);
	}
	
	@Override
	public void onKeyDown(int key, boolean isMouse){
		if(currentScreen == null)//currentScreen == null means that we are ingame
			game.onKeyDown(key, isMouse);
		else //otherwise we are most likely in the pause menu or any other screen that has the game in the background
			currentScreen.onKeyDown(key, isMouse);
	}

	@Override
	public void onKeyRelease(int key, boolean isMouse) {
		if(currentScreen == null)//currentScreen == null means that we are ingame
			game.onKeyRelease(key, isMouse);
		else //otherwise we are most likely in the pause menu or any other screen that has the game in the background
			currentScreen.onKeyRelease(key, isMouse);	
	}
	
	@Override
	public boolean shouldKeyRepeat(int key, boolean isMouse){
		return game.shouldKeyRepeat(key, isMouse);
	}

}
