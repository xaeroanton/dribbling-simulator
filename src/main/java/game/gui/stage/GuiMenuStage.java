package game.gui.stage;

import java.io.IOException;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.opengl.GL11;

import game.Game;
import game.controls.ControlsHandler;
import game.gui.GuiLoading;
import game.gui.GuiMainMenu;
import game.window.GameWindow;
import graphics.window.Window;
import math.vector.Vector3f;

/*
 * For showing GUI screens outside the game
 */

public class GuiMenuStage extends GuiStage {
	
	private Vector3f bg;

	public GuiMenuStage(Window window, Game game) {
		super(window);
		bg = new Vector3f(0, 0, 0);
		openGuiScreen(new GuiLoading(this){
			@Override
			public void init() {
				super.init();
				new Thread(new Runnable(){
					@Override
					public void run() {
						try {
							game.load();
							GameWindow gameWindow = (GameWindow)stage.getWindow();
							gameWindow.showStage(gameWindow.gameStage);
							gameWindow.gameStage.openGuiScreen(new GuiMainMenu(gameWindow.gameStage));
							//stage.openGuiScreen(new GuiMainMenu(stage));
						} catch (IOException e) {
							e.printStackTrace(System.err);
							System.exit(-1);
						}
					}
					
				}).start();
			}
			
		});
	}
	
	@Override
	public void preInit(){
	}

	@Override
	public void init(){
	}
	
	@Override
	public void drawStage(){
		ControlsHandler.serveKeyInputs(this);
		GL11.glClearColor(bg.x, bg.y, bg.z, 1f);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_ONE, GL11.GL_ONE_MINUS_SRC_ALPHA);
		super.drawStage();
	}

	@Override
	public void closeStage() {
		System.out.println("bye!");
	}
	
	@Override
	public void onKeyPress(int key, boolean isMouse) {
		if(currentScreen != null)
			currentScreen.onKeyPress(key, isMouse);
	}

	@Override
	public void onKeyDown(int key, boolean isMouse) {
		if(currentScreen != null)
			currentScreen.onKeyPress(key, isMouse);
	}

	@Override
	public void onKeyRelease(int key, boolean isMouse) {
		if(currentScreen != null)
			currentScreen.onKeyRelease(key, isMouse);
	}

	@Override
	public boolean shouldKeyRepeat(int key, boolean isMouse){
		if(!isMouse){
			if(key == GLFW.GLFW_KEY_ENTER)
				return false;
		} else {

		}
		return true;
	}

}
