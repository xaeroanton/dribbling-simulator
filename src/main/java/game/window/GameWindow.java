package game.window;

import java.awt.Font;
import java.io.IOException;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.opengl.GL11;

import game.controls.ControlsHandler;
import game.gui.stage.GuiGameStage;
import game.gui.stage.GuiMenuStage;
import game.gui.stage.GuiStage;
import graphics.exception.GLException;
import graphics.exception.ShaderException;
import graphics.font.FontRenderer;
import graphics.shaders.ShaderProgram;
import graphics.window.Window;

public class GameWindow extends Window {
	
	public static ShaderProgram normalShaders;

	private GuiStage newStage;
	private GuiStage currentStage;
	public GuiGameStage gameStage;
	public GuiMenuStage menuStage;
	
	private long lastFPS = 0;
	@SuppressWarnings("unused")
	private int fps;

	public GameWindow() {
		super("Dribbling Simulator", 1000, 600, false, true, true);
        gameStage = new GuiGameStage(this);
        menuStage = new GuiMenuStage(this, gameStage.getGame());
        currentStage = menuStage;
	}

	@Override
	public void init() throws IOException, ShaderException {
		GL11.glViewport(0, 0, getWidth(), getHeight());
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glDepthFunc(GL11.GL_LESS);
        GL11.glEnable(GL11.GL_CULL_FACE);
        normalShaders = new ShaderProgram("normal");
		//fontRenderer = new FontRenderer(new Font("Charlemagne Std", Font.PLAIN, 145), 2048, normalShaders);
		fontRenderer = new FontRenderer(new Font("Courier", Font.PLAIN, 145), 2048, normalShaders);
        fontRenderer.getTexture().upload();
        gameStage.preInit();
        menuStage.preInit();
	}

	@Override
	public void cycle() throws InterruptedException, GLException, ShaderException, IOException {
		if(newStage != null && newStage != currentStage){
			newStage.init();
			currentStage = newStage;
		}
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT); // clear the framebuffer
		
		currentStage.drawStage();
		
		fps++;
		if(System.currentTimeMillis() - lastFPS >= 1000){
			//System.out.println("FPS: " + fps);
			fps = 0;
			lastFPS = System.currentTimeMillis();
		}
		
        GLFW.glfwSwapBuffers(get());//flips the window buffers to now show the new scene that has been rendered, then waits for the necessary time
	}

	@Override
	public void end() {
		gameStage.closeStage();
		menuStage.closeStage();
		if(normalShaders != null)
			normalShaders.unloadProgram();
	}

	@Override
	public void onKeyCallback(int key, int scancode, int action, int mods, boolean isMouse) {
		ControlsHandler.handleKeyInput(key, action, isMouse);
	}
	
	public void showStage(GuiStage stage){
		newStage = stage;
	}

}
