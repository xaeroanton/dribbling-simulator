package game.controls;

import java.util.ArrayList;

import org.lwjgl.glfw.GLFW;

import game.gui.stage.GuiStage;

public class ControlsHandler {

	public static ArrayList<KeyEvent> keyEvents = new ArrayList<KeyEvent>();
	public static ArrayList<KeyEvent> toDelete = new ArrayList<KeyEvent>();

	public static void handleKeyInput(int key, int action, boolean isMouse) {//runs on graphics thread
		if(action == GLFW.GLFW_PRESS || action == GLFW.GLFW_RELEASE)
			keyEvents.add(new KeyEvent(key, action, isMouse));
	}

	public static void serveKeyInputs(GuiStage stage){//runs on game thread
		for(int i = 0; i < keyEvents.size(); i++){
			KeyEvent e = keyEvents.get(i);
			if(e.action == GLFW.GLFW_PRESS){
				if(!e.pressedOnce){
					stage.onKeyPress(e.key, e.isMouse);
					e.pressedOnce = true;
				}
				stage.onKeyDown(e.key, e.isMouse);
				if(!stage.shouldKeyRepeat(e.key, e.isMouse))//if we only need to respond to the key being pressed once
					keyEvents.add(new KeyEvent(e.key, GLFW.GLFW_RELEASE, e.isMouse));
			} else if(e.action == GLFW.GLFW_RELEASE){
				stage.onKeyRelease(e.key, e.isMouse);
				removeEvents(e.key);
			}
		}
		keyEvents.removeAll(toDelete);
		toDelete.clear();
	}

	private static void removeEvents(int key){
		for(int i = 0; i < keyEvents.size(); i++){
			KeyEvent e = keyEvents.get(i);
			if(e.key == key)
				toDelete.add(e);
		}
	}

}
