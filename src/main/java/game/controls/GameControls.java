package game.controls;

import org.lwjgl.glfw.GLFW;

import game.Game;
import game.gui.GuiPause;

public class GameControls {
	
	private Game game;
	private boolean mouseControl = false;
	public boolean gameMode = true;
	
	public GameControls(Game game){
		this.game = game;
	}	
	
	public boolean shouldKeyRepeat(int key, boolean isMouse){
		if(!isMouse){
			if(key == GLFW.GLFW_KEY_ESCAPE)
				return false;
		} else {

		}
		return true;
	}
	
	public void onKeyPress(int key){//called once on press
		if(key == GLFW.GLFW_KEY_ESCAPE){
			game.pause();
		}
		if(key == GLFW.GLFW_KEY_SPACE)
			toggleCameraRestriction();
		if(key == GLFW.GLFW_KEY_T)
			if(game.getCurrentCamera() == game.getMainCamera())
				game.setCurrentCamera(game.getSunShadowMapper());
			else
				game.setCurrentCamera(game.getMainCamera());
		if(key == GLFW.GLFW_KEY_R)
			game.startGame();
	}
	
	public void onKeyDown(int key){//called every tick when down
		/*if(key == GLFW.GLFW_KEY_LEFT)
			game.getMainCamera().addToRotation(0, 7, 0);
		else if(key == GLFW.GLFW_KEY_RIGHT)
			game.getMainCamera().addToRotation(0, -7, 0);
		else if(key == GLFW.GLFW_KEY_UP)
			game.getMainCamera().addToRotation(7, 0, 0);
		else if(key == GLFW.GLFW_KEY_DOWN)
			game.getMainCamera().addToRotation(-7, 0, 0);*/
		if (!gameMode){
			if(key == GLFW.GLFW_KEY_W)
				game.getMainCamera().addToPosition(game.getMainCamera().getLookVector().scale(0.3f));
			else if(key == GLFW.GLFW_KEY_S)
				game.getMainCamera().addToPosition(game.getMainCamera().getLookVector().negative().scale(0.3f));
			else if(key == GLFW.GLFW_KEY_A)
				game.getMainCamera().addToPosition(game.getMainCamera().getLeftVector().scale(0.3f));
			else if(key == GLFW.GLFW_KEY_D)
				game.getMainCamera().addToPosition(game.getMainCamera().getRightVector().scale(0.3f));
		}else{
			if(key == GLFW.GLFW_KEY_A)
				game.getMainCamera().addToPosition(-0.5f, 0, 0);
			else if(key == GLFW.GLFW_KEY_D)
				game.getMainCamera().addToPosition(0.5f, 0, 0);
		}
	}
	
	public void onKeyRelease(int key){//called once on release
		
	}
	
	public void onMousePress(int button){//called once on press
		if(button == GLFW.GLFW_MOUSE_BUTTON_RIGHT){
			if(!gameMode)
				enableMouseControl();
		}
	}
	
	public void onMouseDown(int button){//called every tick when down
		if(button == GLFW.GLFW_MOUSE_BUTTON_LEFT)
			game.getBall().forceMovement(game);
	}
	
	public void onMouseRelease(int button){//called once on release
		if(button == GLFW.GLFW_MOUSE_BUTTON_RIGHT){
			if(!gameMode)
				disableMouseControl();
		}
	}
	
	public void enableMouseControl(){
		if(!mouseControl){
			game.getStage().getWindow().cursorToOrigin();
			game.getStage().getWindow().lockCursor();
			mouseControl = true;
		}
	}
	
	public void disableMouseControl(){
		game.getStage().getWindow().unlockCursor();
		mouseControl = false;
	}
	
	public void rotateCamera(){
		if(mouseControl){
			float mouseX = (float) game.getStage().getWindow().getMouseX();
			float mouseY = (float) game.getStage().getWindow().getMouseY();
			float distance = (float) Math.sqrt(mouseX * mouseX + mouseY * mouseY);
			if(distance > 360){//limit the mouse speed
				mouseX = mouseX * 360.0f / distance;
				mouseY = mouseY * 360.0f / distance;
			}
			game.getMainCamera().addToRotation(-mouseY / 4, -mouseX / 4, 0);
			// x limitations
			if (game.getMainCamera().getRotation().x > 90)
				game.getMainCamera().setRotation(90, game.getMainCamera().getRotation().y, game.getMainCamera().getRotation().z);
			else if (game.getMainCamera().getRotation().x < -90)
				game.getMainCamera().setRotation(-90, game.getMainCamera().getRotation().y, game.getMainCamera().getRotation().z);
			if (gameMode) { // y limitations
				if (game.getMainCamera().getRotation().y > 90)
					game.getMainCamera().setRotation(game.getMainCamera().getRotation().x, 90, game.getMainCamera().getRotation().z);
				else if (game.getMainCamera().getRotation().y < -90)
					game.getMainCamera().setRotation(game.getMainCamera().getRotation().x, -90, game.getMainCamera().getRotation().z);
			}
			game.getStage().getWindow().cursorToOrigin();
		}
	}
	
	public void toggleCameraRestriction(){
		gameMode = !gameMode;
		if (gameMode)
			game.getMainCamera().setRotation(0, 0, 0);
	}		
	
}
