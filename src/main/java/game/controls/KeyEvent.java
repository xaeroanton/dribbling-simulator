package game.controls;

public class KeyEvent {
	
	int key;
	int action;
	boolean isMouse;
	boolean pressedOnce = false;
	
	public KeyEvent(int key, int action, boolean isMouse){
		this.key = key;
		this.action = action;
		this.isMouse = isMouse;
	}

}
