package graphics.gui;

import game.gui.stage.GuiStage;
import graphics.object.model.Model;
import graphics.object.model.Vertex;
import graphics.texture.Texture;
import math.vector.Vector2f;
import math.vector.Vector3f;
import math.vector.Vector4f;

public class Gui {
	
	public static Model createTexturedRect(GuiStage stage, Texture texture, int left, int top, int right, int bottom, float r, float g, float b, float a){
		Model rect = new Model(texture);
		Vector4f rectColour = new Vector4f(r, g, b, a);
		rect.addVertex(new Vertex(left, top, 0, rectColour, new Vector2f(0, 1), new Vector3f(0, 0, 1)));
		rect.addVertex(new Vertex(left, bottom, 0, rectColour, new Vector2f(0, 0), new Vector3f(0, 0, 1)));
		rect.addVertex(new Vertex(right, top, 0, rectColour, new Vector2f(1, 1), new Vector3f(0, 0, 1)));
		rect.addVertex(new Vertex(right, bottom, 0, rectColour, new Vector2f(1, 0), new Vector3f(0, 0, 1)));
		rect.addTriangle(0, 1, 2);
		rect.addTriangle(2, 1, 3);
		return rect;
	}
	
	public static Model createRect(GuiStage stage, int left, int top, int right, int bottom, float r, float g, float b, float a){
		Model rect = new Model();
		Vector4f rectColour = new Vector4f(r, g, b, a);
		rect.addVertex(new Vertex(left, top, 0, rectColour));
		rect.addVertex(new Vertex(left, bottom, 0, rectColour));
		rect.addVertex(new Vertex(right, top, 0, rectColour));
		rect.addVertex(new Vertex(right, bottom, 0, rectColour));
		rect.addTriangle(0, 1, 2);
		rect.addTriangle(2, 1, 3);
		return rect;
	}
}
