package graphics.texture;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;

import graphics.exception.GLException;

public class FrameBufferAA extends FrameBuffer {
	
	private int AAlocation;//location of the multisampled frame buffer
	private int AALevel;
	private int AAcolourRBO;
	private int AAdepthRBO;

	public FrameBufferAA(Texture texture, int level) throws GLException {
		super(texture);
		this.AALevel = level;
	}
	
	@Override
	public void init() throws GLException{
		super.init();
		//creating a renderbuffer for colour info
		AAcolourRBO = GL30.glGenRenderbuffers();
		GL30.glBindRenderbuffer(GL30.GL_RENDERBUFFER, AAcolourRBO);
		GL30.glRenderbufferStorageMultisample(GL30.GL_RENDERBUFFER, AALevel, GL11.GL_RGB8, texture.getWidth(), texture.getHeight());
		
		//creating a renderbuffer for storing depth info
		AAdepthRBO = GL30.glGenRenderbuffers();
		GL30.glBindRenderbuffer(GL30.GL_RENDERBUFFER, AAdepthRBO);
		GL30.glRenderbufferStorageMultisample(GL30.GL_RENDERBUFFER, AALevel, GL11.GL_DEPTH_COMPONENT, texture.getWidth(), texture.getHeight());

		AAlocation = GL30.glGenFramebuffers();
		GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, AAlocation);
		//attaching render buffers to the frame buffer
		GL30.glFramebufferRenderbuffer(GL30.GL_FRAMEBUFFER, GL30.GL_COLOR_ATTACHMENT0, GL30.GL_RENDERBUFFER, AAcolourRBO);
		GL30.glFramebufferRenderbuffer(GL30.GL_FRAMEBUFFER, GL30.GL_DEPTH_ATTACHMENT, GL30.GL_RENDERBUFFER, AAdepthRBO);
		if(GL30.glCheckFramebufferStatus(GL30.GL_FRAMEBUFFER) != GL30.GL_FRAMEBUFFER_COMPLETE)
			throw new GLException("Error initializing AA frame buffer: " + AAlocation);
		unbind();
	}
	
	public void blit(){
		GL30.glBindFramebuffer(GL30.GL_READ_FRAMEBUFFER, AAlocation);
		GL30.glBindFramebuffer(GL30.GL_DRAW_FRAMEBUFFER, getLocation());
		GL30.glBlitFramebuffer(0, 0, getTexture().getWidth(), getTexture().getHeight(), 
				0, 0, getTexture().getWidth(), getTexture().getHeight(), GL11.GL_COLOR_BUFFER_BIT, GL11.GL_LINEAR);
	}
	
	public void bind(){
		GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, AAlocation);
	}
	
	public void destroy(){
		super.destroy();
		GL30.glDeleteRenderbuffers(AAcolourRBO);
		GL30.glDeleteRenderbuffers(AAdepthRBO);
		GL30.glDeleteFramebuffers(AAlocation);
	}

}
