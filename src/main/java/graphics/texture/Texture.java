package graphics.texture;

import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import javax.imageio.ImageIO;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL14;

public class Texture {
	
	private int texture; //OpenGL object
	private int width;
	private int height;
	private ByteBuffer imageBuffer;
	
	public Texture(String name) throws IOException {
		/*
		 * Loads a texture from an image file
		 * Doesn't require GL context
		 */
		InputStream imageIn = getClass().getResourceAsStream(name);
		if(imageIn == null)
			throw new FileNotFoundException("Couldn't load a texture: " + name);
		BufferedImage image = ImageIO.read(imageIn);
		imageIn.close();
		createBuffer(image);
	}
	
	public Texture(BufferedImage image){
		createBuffer(image);
	}
	
	public Texture(int width, int height){//new empty texture
		/*
		 * Creates an empty OpenGL texture object
		 * Requires GL context
		 */
		this.width = width;
		this.height = height;
		upload();
	}

	private void createBuffer(BufferedImage image){
		width = image.getWidth();
		height = image.getHeight();
		int[] argbArray = new int[width * height];
		image.getRGB(0, 0, width, height, argbArray, 0, width);//returns colours in argb format
		imageBuffer = BufferUtils.createByteBuffer(width * height * 4);

		for(int i = 0; i < argbArray.length; i++){
			int argb = argbArray[i];
			int alpha = (argb >> 24) & 255;
			imageBuffer.put((byte) (((argb >> 16) & 255) * alpha / 255));//red
			imageBuffer.put((byte) (((argb >> 8) & 255) * alpha / 255));//green
			imageBuffer.put((byte) ((argb & 255) * alpha / 255));//blue
			imageBuffer.put((byte) alpha);//alpha
		}
		imageBuffer.flip();
	}

	public void upload(){
		/*
		 * Uploads the texture data to OpenGL
		 * Requires GL context
		 */
		//creating the texture object
		this.texture = GL11.glGenTextures();
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, this.texture);
		//setting required parametres
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		//uploading texture data
		if(imageBuffer != null)
			GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA, this.width, this.height, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, imageBuffer);
		//unbinding the texture
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
		imageBuffer = null;
	}
	
	public void bind(int activeTexture){//0 for normal texture, 2 for shadow maps
		GL13.glActiveTexture(GL13.GL_TEXTURE0 + activeTexture);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, texture);
	}
	
	public static void unbind(int activeTexture){
		GL13.glActiveTexture(GL13.GL_TEXTURE0 + activeTexture);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
	}
	
	public int get(){
		return texture;
	}
	
	public int getWidth(){
		return width;
	}
	
	public int getHeight(){
		return height;
	}

}
