package graphics.texture;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL14;
import org.lwjgl.opengl.GL30;
import org.lwjgl.system.MemoryUtil;

import graphics.exception.GLException;

public class FrameBuffer {
	
	protected Texture texture;
	private int location;
	private int depthRBO;
	
	public FrameBuffer(Texture texture) throws GLException{
		location = GL30.glGenFramebuffers();
		this.texture = texture;
	}
	
	public void init() throws GLException {
		texture.bind(0);
		GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA8, texture.getWidth(), texture.getHeight(), 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, MemoryUtil.NULL);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR_MIPMAP_LINEAR);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL14.GL_GENERATE_MIPMAP, GL11.GL_TRUE);//automatic mipmap
		Texture.unbind(0);
		
		depthRBO = GL30.glGenRenderbuffers();
		GL30.glBindRenderbuffer(GL30.GL_RENDERBUFFER, depthRBO);
		GL30.glRenderbufferStorage(GL30.GL_RENDERBUFFER, GL11.GL_DEPTH_COMPONENT, texture.getWidth(), texture.getHeight());
		GL30.glBindRenderbuffer(GL30.GL_RENDERBUFFER, 0);

		GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, location);
		GL30.glFramebufferTexture2D(GL30.GL_FRAMEBUFFER, GL30.GL_COLOR_ATTACHMENT0, GL11.GL_TEXTURE_2D, texture.get(), 0);
		GL30.glFramebufferRenderbuffer(GL30.GL_FRAMEBUFFER, GL30.GL_DEPTH_ATTACHMENT, GL30.GL_RENDERBUFFER, depthRBO);
		if(GL30.glCheckFramebufferStatus(GL30.GL_FRAMEBUFFER) != GL30.GL_FRAMEBUFFER_COMPLETE)
			throw new GLException("Error initializing normal frame buffer: " + location + " " + texture.get());
		unbind();
	}

	public void bind(){
		GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, location);
	}
	
	public static void unbind(){
		GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, 0);
	}
	
	public Texture getTexture(){
		return texture;
	}
	
	public int getLocation(){
		return location;
	}
	
	public void destroy(){
		GL30.glDeleteRenderbuffers(depthRBO);
		GL30.glDeleteFramebuffers(location);
	}

}
