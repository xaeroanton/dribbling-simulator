package graphics.window;

import java.io.IOException;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GLCapabilities;

import graphics.exception.GLException;
import graphics.exception.ShaderException;
import graphics.font.FontRenderer;
import graphics.object.Camera;
import math.matrix.OrthogonalMatrix;

public abstract class Window implements Runnable {
	
	private long window;//OpenGL object
	private String title;
	int width;
	int height;
	private double mouseX;
	private double mouseY;
	private boolean resizable;
	private boolean center;
	private boolean fullscreen;
    protected GLCapabilities capabilities;
    public FontRenderer fontRenderer;
    public Camera guiCamera;
    public boolean shouldClose;
	
	public Window(String title, int width, int height, boolean resizable, boolean center, boolean fullscreen){
		this.resizable = resizable;
		this.center = center;
		this.title = title;
		this.width = width;
		this.height = height;
		this.shouldClose = false;
		this.fullscreen = fullscreen;
	}
	
	@Override
	public void run() {
		window = WindowHandler.createWindow(this, center);
		guiCamera = new Camera(new OrthogonalMatrix(0, width, height, 0, 0, 100));
		WindowHandler.makeContextCurrent(this);
		capabilities = GL.createCapabilities();//creates the GLCapabilities instance and makes OpenGL bindings available for use
		
		try {
			init();
			GLFW.glfwShowWindow(window);
			while (!GLFW.glfwWindowShouldClose(window) && !shouldClose){//runs until the X button is clicked
				cycle();
				GLFW.glfwPollEvents();//processes all pending window events, for example key callback
			}
		} catch(Exception e){//in case of an exception we should still reach the window.close() line before crashing
			e.printStackTrace(System.err);
		}
		end();
		close();
	}
	
	public void cursorToOrigin(){//move cursor to top left
		mouseX = 0;
		mouseY = 0;
		GLFW.glfwSetCursorPos(window, 0, 0);
	}
	
	public void lockCursor(){
		GLFW.glfwSetInputMode(window, GLFW.GLFW_CURSOR, GLFW.GLFW_CURSOR_DISABLED);
	}
	
	public void unlockCursor(){
		GLFW.glfwSetInputMode(window, GLFW.GLFW_CURSOR, GLFW.GLFW_CURSOR_NORMAL);
		GLFW.glfwSetCursorPos(window, width/2, height/2);
	}
	
	public abstract void init() throws IOException, ShaderException, GLException;//what to do before running the cycles
	public abstract void cycle() throws InterruptedException, GLException, ShaderException, IOException;//what to do every draw cycle
	public abstract void end();//what to do before closing
	public abstract void onKeyCallback(int key, int scancode, int action, int mods, boolean isMouse);

	public void close(){
		GLFW.glfwDestroyWindow(window);
	}
	
	public int getHeight() {
		return height;
	}
	
	public int getWidth() {
		return width;
	}
	
	public String getTitle() {
		return title;
	}
	
	public boolean isResizable(){
		return resizable;
	}
	
	public long get() {
		return window;
	}

	public double getMouseX() {
		return mouseX;
	}

	void setMouseX(double mouseX) {
		this.mouseX = mouseX;
	}

	public double getMouseY() {
		return mouseY;
	}

	void setMouseY(double mouseY) {
		this.mouseY = mouseY;
	}
	
	public boolean isFullscreen(){
		return fullscreen;
	}

}
