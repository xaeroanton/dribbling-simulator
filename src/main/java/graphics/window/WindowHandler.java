package graphics.window;

import static org.lwjgl.opengl.GL11.GL_FALSE;
import static org.lwjgl.opengl.GL11.GL_TRUE;
import static org.lwjgl.system.MemoryUtil.NULL;

import java.util.HashMap;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;
import org.lwjgl.glfw.GLFWVidMode;

public class WindowHandler {
	
	public static GLFWErrorCallback errorCallback;
    private static GLFWKeyCallback keyCallback;
    private static GLFWCursorPosCallback cursorPosCallback;
    private static GLFWMouseButtonCallback mouseButtonCallback;
    private static HashMap<Long, Window> windows = new HashMap<Long, Window>();//window id -> Window object
	
	public static void init() throws IllegalStateException {//initializes GLFW
		errorCallback = GLFWErrorCallback.createPrint(System.err);
		GLFW.glfwSetErrorCallback(errorCallback);
		if(!GLFW.glfwInit())
            throw new IllegalStateException("Unable to initialize GLFW");		
		//setting up key callback
		keyCallback = new GLFWKeyCallback(){
			@Override
			public void invoke(long windowId, int key, int scancode, int action, int mods) {
				Window window = windows.get(windowId);
				window.onKeyCallback(key, scancode, action, mods, false);
			}
		};
		//setting up cursor callback
		cursorPosCallback = new GLFWCursorPosCallback(){
			@Override
			public void invoke(long windowId, double xpos, double ypos) {
				Window window = windows.get(windowId);
				window.setMouseX(xpos);
				window.setMouseY(ypos);
			}
		};
		//setting up mouse button callback
		mouseButtonCallback = new GLFWMouseButtonCallback(){
			@Override
			public void invoke(long windowId, int button, int action, int mods) {
				Window window = windows.get(windowId);
				window.onKeyCallback(button, -1, action, mods, true);
			}
		};
	}
	
	static long createWindow(Window window, boolean center){
		GLFW.glfwDefaultWindowHints();//set window hints to default even though they are already
		GLFW.glfwWindowHint(GLFW.GLFW_VISIBLE, GL_FALSE);//make the window invisible for now
		GLFW.glfwWindowHint(GLFW.GLFW_RESIZABLE, window.isResizable() ? GL_TRUE : GL_FALSE);
		GLFWVidMode vidMode = GLFW.glfwGetVideoMode(GLFW.glfwGetPrimaryMonitor());
		if(window.isFullscreen()){
			window.width = vidMode.width();
			window.height = vidMode.height();
			System.out.println(window.width);
		}
		long id = GLFW.glfwCreateWindow(window.getWidth(), window.getHeight(), window.getTitle(), window.isFullscreen() ? GLFW.glfwGetPrimaryMonitor(): NULL, NULL);
		if(id == NULL)
            throw new RuntimeException("Failed to create the GLFW window");
		if(center)
			GLFW.glfwSetWindowPos(id, (vidMode.width() - window.getWidth()) / 2, (vidMode.height() - window.getHeight()) / 2);
		GLFW.glfwSetKeyCallback(id, keyCallback);
		GLFW.glfwSetCursorPosCallback(id, cursorPosCallback);
		GLFW.glfwSetMouseButtonCallback(id, mouseButtonCallback);
		windows.put(id, window);
		return id;
	}
	
	public static void makeContextCurrent(Window window){//makes the context of a specified window the current context
		GLFW.glfwMakeContextCurrent(window.get());
		GLFW.glfwSwapInterval(1);
	}

}
