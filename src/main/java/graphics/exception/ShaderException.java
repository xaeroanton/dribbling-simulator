package graphics.exception;

import org.lwjgl.opengl.GL20;

public class ShaderException extends Exception {
	
	private static final long serialVersionUID = 1L;
	
	public ShaderException(String message, int shader){
		super(message, new Throwable("\n" + GL20.glGetShaderInfoLog(shader)));
	}

}
