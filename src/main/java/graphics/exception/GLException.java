package graphics.exception;

public class GLException extends Exception {
	
	private static final long serialVersionUID = 1L;
	
	public GLException(String message){
		super(message);
	}

}
