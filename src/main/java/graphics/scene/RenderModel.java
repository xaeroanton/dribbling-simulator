package graphics.scene;

import graphics.object.model.Model;
import graphics.shaders.ShaderProgram;
import math.matrix.Matrix4f;

/*
 * A model that needs to rendered using a certain shader program
 * Needed for rendering the same scene to multiple frame buffers (for example shadow mapping)
 */

public class RenderModel {

	private Model model;
	public ShaderProgram shaders;

	public RenderModel(Model model, ShaderProgram shaders){
		this.model = model;
		this.shaders = shaders;
	}
	
	public Matrix4f render(ShaderProgram currentShaders, Matrix4f viewMatrix, boolean mapping){
		//returns model-view matrix
		return model.render(currentShaders, viewMatrix, mapping);
	}
	
	public void saveBiasDepthMVP(Matrix4f biasDepthMVP){
		model.saveBiasDepthMVP(biasDepthMVP);
	}

}
