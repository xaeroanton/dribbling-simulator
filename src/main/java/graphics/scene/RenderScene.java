package graphics.scene;

import java.nio.FloatBuffer;
import java.util.ArrayList;

import org.lwjgl.BufferUtils;

import graphics.object.Camera;
import graphics.object.ShadowMapper;
import graphics.shaders.ShaderProgram;
import math.matrix.Matrix4f;
import math.matrix.ProjectionMatrix;
import math.vector.Vector3f;

public class RenderScene {

	private ArrayList<RenderModel> modelsToRender;
	private Matrix4f biasMatrix;//converts homogeneous coordinates to texture coordinates
	
	public RenderScene(){
		modelsToRender = new ArrayList<RenderModel>();
		biasMatrix = new Matrix4f();
		biasMatrix.scale(0.5f, 0.5f, 0.5f);
		biasMatrix.translate(1.0f, 1.0f, 1.0f);
	}
	
	public void renderScene(Camera camera, ShaderProgram overrideShaders, ShadowMapper sunShadowMapper, boolean mapping){//overrideShaders variable is useful for shadow mapping
		FloatBuffer matrix44Buffer = BufferUtils.createFloatBuffer(16);
		ShaderProgram previousProgram = null;
		Vector3f directionToSun = null;
		if(!mapping && sunShadowMapper != null)
			directionToSun = sunShadowMapper.getDirection().negative().multiplyByAMatrix(camera.getDirectionMatrix()).normalize();
		for(RenderModel rm : modelsToRender){
			ShaderProgram currentShaders = overrideShaders != null ? overrideShaders : rm.shaders;
			ProjectionMatrix projectionMatrix = camera.getProjectionMatrix();
			if(currentShaders != previousProgram){
				previousProgram = currentShaders;
				currentShaders.use();
				projectionMatrix.upload(matrix44Buffer, currentShaders.getProjectionMatrixLocation());
				currentShaders.uploadTextureUniforms();
				if(!mapping && sunShadowMapper != null){
					sunShadowMapper.bindShadowMap(currentShaders);
					currentShaders.uploadSunDirection(directionToSun);
				}
			}
			Matrix4f modelViewMatrix = rm.render(currentShaders, camera.getViewMatrix(), mapping);
			if(mapping){//we are currently mapping shadows
				Matrix4f biasDepthMVP = modelViewMatrix.multipliedByALeftMatrix((Matrix4f)projectionMatrix).multipliedByALeftMatrix(biasMatrix);
				rm.saveBiasDepthMVP(biasDepthMVP);
			}
		}
	}

	public void add(RenderModel renderModel) {
		modelsToRender.add(renderModel);
	}
	
	public void clear(){
		modelsToRender.clear();
	}
	
}
