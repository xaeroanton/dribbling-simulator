package graphics.font;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.util.HashMap;

import graphics.object.model.ModelString;
import graphics.shaders.ShaderProgram;
import graphics.texture.Texture;
import math.vector.Vector4f;

public class FontRenderer {
	
	private Texture fontTexture;
	private HashMap<Character, int[]> characterLocations;
	private int fontHeight;
	private int ascent;
	private int descent;
	private int size;
	private FontMetrics fm;
	
	public FontRenderer(Font font, int size, ShaderProgram defaultShaders){
		BufferedImage image = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
		this.characterLocations = new HashMap<Character, int[]>();
		this.size = size;
		Graphics2D graphics = (Graphics2D) image.getGraphics();
		graphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		graphics.setFont(font);
		this.fm = graphics.getFontMetrics(font);
		this.ascent = fm.getMaxAscent() + fm.getLeading();
		this.descent = Math.abs(fm.getMaxDescent()) + fm.getLeading();
		this.fontHeight = this.ascent + this.descent;
		int offsetX = 0;
		int line = 0;
		for(int i = 0; i < 256; i++){//drawing first 256 characters to the font texture
			char c = (char) i;
			int charWidth = fm.charWidth(c);
			if(font.canDisplay(c)){
				if(offsetX + charWidth >= size){
					offsetX = 0;
					line++;
				}
				graphics.drawString(Character.toString(c), offsetX, (line + 1) * this.fontHeight - this.descent);
				this.characterLocations.put(c, new int[]{offsetX, line, fm.charWidth(c)});
				offsetX += charWidth + 5;//includes error
			}
		}
		fontTexture = new Texture(image);
	}
	
	public Texture getTexture(){
		return fontTexture;
	}
	
	public int[] getCharLocation(char c){
		return characterLocations.get(c);
	}
	
	public ModelString createString(String text, Vector4f colour){
		return new ModelString(text, colour, this);
	}
	
	public int getSize(){
		return size;
	}
	
	public int getHeight(){
		return this.fontHeight;
	}
	
	public int getStringWidth(String text){
		return fm.stringWidth(text);
	}
	
	public int getAscent(){
		return ascent;
	}
	
	public int getDescent(){
		return descent;
	}

}
