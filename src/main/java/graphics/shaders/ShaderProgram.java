package graphics.shaders;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

import graphics.exception.ShaderException;
import math.vector.Vector3f;

public class ShaderProgram {
	private int program;
	private int vertexShader;
	private int fragmentShader;
	
	private int projectionMatrixLocation;
	private int modelViewMatrixLocation;
	private int normalMatrixLocation;
	private int textureUniformLocation;
	private int useTextureUniformLocation;

	private int biasDepthMVPLocation;
	private int shadowMapLocation;
	private int directionToSunLocation;
	private int shadowMapWidthLocation;
	private int shadowMapHeightLocation;
	
	public ShaderProgram(String name) throws IOException, ShaderException {
		//loading the vertex shaders source
		StringBuilder vertexShaderSource = new StringBuilder();
		InputStream vertexShaderInput = getClass().getResourceAsStream(name + "/vertex.glsl");//gets a resource from the package graphics.shaders ("resources" source folder)
		if(vertexShaderInput == null)
			throw new FileNotFoundException("Vertex shaders source file not found for the shader program: " + name);
		BufferedReader reader = new BufferedReader(new InputStreamReader(vertexShaderInput));
		String line;
		while((line=reader.readLine()) != null)
			vertexShaderSource.append(line).append("\n");
		reader.close();

		//loading the fragment shaders source
		StringBuilder fragmentShaderSource = new StringBuilder();
		InputStream fragmentShaderInput = getClass().getResourceAsStream(name + "/fragment.glsl");//gets a resource from the package graphics.shaders ("resources" source folder)
		if(fragmentShaderInput == null)
			throw new FileNotFoundException("Fragment shaders source file not found for the shader program: " + name);
		reader = new BufferedReader(new InputStreamReader(fragmentShaderInput));
		line = null;
		while((line=reader.readLine()) != null)
			fragmentShaderSource.append(line).append("\n");
		reader.close();
		
		//creating shader instances
		program = GL20.glCreateProgram();
		vertexShader = GL20.glCreateShader(GL20.GL_VERTEX_SHADER);
		fragmentShader = GL20.glCreateShader(GL20.GL_FRAGMENT_SHADER);

		//uploading and compiling the vertex shader
		GL20.glShaderSource(vertexShader, vertexShaderSource);
		GL20.glCompileShader(vertexShader);
		if(GL20.glGetShaderi(vertexShader, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE)
			throw new ShaderException("Error compiling vertex shaders for shader program: " + name, vertexShader);
		
		//uploading and compiling the fragment shader
		GL20.glShaderSource(fragmentShader, fragmentShaderSource);
		GL20.glCompileShader(fragmentShader);
		if(GL20.glGetShaderi(fragmentShader, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE){
			//System.out.println();
			throw new ShaderException("Error compiling fragment shaders for shader program: " + name, fragmentShader);
		}
		
		//attaching both shaders to the program
		GL20.glAttachShader(program, vertexShader);
		GL20.glAttachShader(program, fragmentShader);
		
		//associating indexes 0 1 2 with corresponding vertex attributes
		GL20.glBindAttribLocation(program, 0, "in_Position");
		GL20.glBindAttribLocation(program, 1, "in_Colour");
		GL20.glBindAttribLocation(program, 2, "in_UV");
		GL20.glBindAttribLocation(program, 3, "in_Normal");
		
		//link and validate
		GL20.glLinkProgram(program);
		GL20.glValidateProgram(program);
		
		//getting matrix uniform locations
		projectionMatrixLocation = GL20.glGetUniformLocation(program, "projectionMatrix");
		modelViewMatrixLocation = GL20.glGetUniformLocation(program, "modelViewMatrix");
		normalMatrixLocation = GL20.glGetUniformLocation(program, "normalMatrix");
		textureUniformLocation = GL20.glGetUniformLocation(program, "texture0");
		useTextureUniformLocation = GL20.glGetUniformLocation(program, "useTexture");
		
		biasDepthMVPLocation = GL20.glGetUniformLocation(program, "biasDepthMVP");
		shadowMapLocation = GL20.glGetUniformLocation(program, "shadowMap");
		directionToSunLocation = GL20.glGetUniformLocation(program, "directionToSun");
		shadowMapWidthLocation = GL20.glGetUniformLocation(program, "shadowMapWidth");
		shadowMapHeightLocation = GL20.glGetUniformLocation(program, "shadowMapHeight");
	}
	
	public void use(){
		GL20.glUseProgram(program);
	}
	
	public void unloadProgram(){
		GL20.glDeleteProgram(program);
		GL20.glDeleteShader(vertexShader);
		GL20.glDeleteShader(fragmentShader);
	}

	public int getProjectionMatrixLocation() {
		return projectionMatrixLocation;
	}
	
	public int getModelViewMatrixLocation() {
		return modelViewMatrixLocation;
	}
	
	public int getNormalMatrixLocation() {
		return normalMatrixLocation;
	}
	
	public int getUseTextureUniformLocation() {
		return useTextureUniformLocation;
	}
	
	public int getBiasDepthMVPLocation() {
		return biasDepthMVPLocation;
	}

	public int getShadowMapWidthLocation() {
		return shadowMapWidthLocation;
	}

	public int getShadowMapHeightLocation() {
		return shadowMapHeightLocation;
	}
	
	public void uploadSunDirection(Vector3f dir){
		GL20.glUniform3f(directionToSunLocation, dir.x, dir.y, dir.z);
	}

	public void uploadTextureUniforms() {
		GL20.glUniform1i(textureUniformLocation, 0);
		GL20.glUniform1i(shadowMapLocation, 1);
	}
}
