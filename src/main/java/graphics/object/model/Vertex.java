package graphics.object.model;

import math.vector.Vector2f;
import math.vector.Vector3f;
import math.vector.Vector4f;

public class Vertex {

	public static final int SIZE = 12;//size of a vertex in floats
	public static final int STRIDE = SIZE * 4;//size of a vertex in bytes
	private Vector3f position;
	private Vector4f colour;
	private Vector2f textureUV;
	private Vector3f normal;
	
	public Vertex(float x, float y, float z, Vector4f colour){
		this(new Vector3f(x, y, z), colour, new Vector2f(0, 0), new Vector3f(0, 0, 0));
	}
	
	public Vertex(float x, float y, float z, float r, float g, float b, float a){
		this(new Vector3f(x, y, z), new Vector4f(r, g, b, a), new Vector2f(0, 0), new Vector3f(0, 0, 0));
	}

	public Vertex(float x, float y, float z, float r, float g, float b, float a, float u, float v, float nx, float ny, float nz){
		this(new Vector3f(x, y, z), new Vector4f(r, g, b, a), new Vector2f(u, v), new Vector3f(nx, ny, nz));
	}
	
	public Vertex(float x, float y, float z, Vector4f colour, Vector2f textureUV, Vector3f normal){
		this(new Vector3f(x, y, z), colour, textureUV, normal);
	}
	
	public Vertex(Vector3f position, Vector4f colour, Vector2f textureUV, Vector3f normal){
		this.position = position;
		this.colour = colour;
		this.textureUV = textureUV;
		this.normal = normal;
	}
	
	public float[] getData(){
		float[] data = new float[SIZE];
		System.arraycopy(position.getXYZ(), 0, data, 0, 3);
		System.arraycopy(colour.getRGBA(), 0, data, 3, 4);
		System.arraycopy(textureUV.getXY(), 0, data, 7, 2);
		System.arraycopy(normal.getXYZ(), 0, data, 9, 3);
		return data;
	}
	
	public Vector3f getPosition(){
		return new Vector3f(position);
	}
	
	public void setPosition(float x, float y, float z){
		position.x = x;
		position.y = y;
		position.z = z;
	}

}
