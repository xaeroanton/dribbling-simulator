package graphics.object.model;

import graphics.font.FontRenderer;
import graphics.scene.RenderScene;
import graphics.shaders.ShaderProgram;
import math.vector.Vector2f;
import math.vector.Vector3f;
import math.vector.Vector4f;

public class ModelString extends Model {
	
	private String text;
	private int width;
	private Vector4f colour;
	private FontRenderer fontRenderer;
	
	public ModelString(String text, Vector4f colour, FontRenderer fontRenderer){
		super(fontRenderer.getTexture());
		this.text = text;
		this.width = fontRenderer.getStringWidth(this.text);
		this.colour = colour;
		this.fontRenderer = fontRenderer;
		char[] chars = this.text.toCharArray();
		int offset = 0;
		for(char c : chars){
			int[] loc = fontRenderer.getCharLocation(c);
			if(loc != null){
				addChar(fontRenderer, loc, offset);
				offset += loc[2];
			}
		}
	}
	
	private void addChar(FontRenderer fr, int[] location, int offset){
		int baseLine = fontRenderer.getHeight() * (location[1] + 1) - fontRenderer.getDescent();
		float xCoordLeft = (float)(location[0]) / fontRenderer.getSize();
		float yCoordTop = (float)(baseLine - fontRenderer.getAscent()) / fontRenderer.getSize();
		float xCoordRight = (float)(location[0] + location[2]) / fontRenderer.getSize();
		float yCoordBottom = (float)(baseLine + fontRenderer.getDescent()) / fontRenderer.getSize();
		
		int vertex0 = addVertex(new Vertex(offset, 0, 0, colour, new Vector2f(xCoordLeft, yCoordTop), new Vector3f(0, 0, 1)));
		int vertex1 = addVertex(new Vertex(offset, -fontRenderer.getHeight() + fontRenderer.getDescent(), 0, colour, new Vector2f(xCoordLeft, yCoordBottom), new Vector3f(0, 0, 1)));
		int vertex2 = addVertex(new Vertex(offset + location[2], 0, 0, colour, new Vector2f(xCoordRight, yCoordTop), new Vector3f(0, 0, 1)));
		int vertex3 = addVertex(new Vertex(offset + location[2], -fontRenderer.getHeight() + fontRenderer.getDescent(), 0, colour, new Vector2f(xCoordRight, yCoordBottom), new Vector3f(0, 0, 1)));
		
		addTriangle(vertex0, vertex1, vertex2);
		addTriangle(vertex2, vertex1, vertex3);
	}
	
	public void addCenteredToScene(RenderScene scene, ShaderProgram shaders, Vector3f position, Vector3f rotation, Vector3f scale, boolean centerY){
		Vector3f pos = new Vector3f(position);
		pos.x -= width / 2.0f * scale.x;
		if(centerY)
			pos.y += fontRenderer.getHeight() / 2.0f * scale.y;
		addToScene(scene, shaders, pos, rotation, scale);
	}

}
