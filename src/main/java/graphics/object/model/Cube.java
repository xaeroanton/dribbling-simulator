package graphics.object.model;

import graphics.texture.Texture;
import math.vector.Vector2f;
import math.vector.Vector3f;
import math.vector.Vector4f;

public class Cube extends Model {
	
	public Cube(Vector4f colour1, Vector4f colour2){
		this(null, colour1, colour2);
	}
	
	public Cube(Texture texture, Vector4f colour1, Vector4f colour2){
		super(texture);
		addVertices(new Vertex[] {
				//bottom face
				new Vertex(-0.5f, -0.5f, -0.5f, colour1, new Vector2f(0, 0), new Vector3f(0, -1, 0)),		//0
				new Vertex(0.5f, -0.5f, -0.5f, colour1, new Vector2f(1, 0), new Vector3f(0, -1, 0)),		//1
				new Vertex(0.5f, -0.5f, 0.5f, colour1, new Vector2f(1, 1), new Vector3f(0, -1, 0)),			//2
				new Vertex(-0.5f, -0.5f, 0.5f, colour1, new Vector2f(0, 1), new Vector3f(0, -1, 0)),		//3
				
				//top face
				new Vertex(-0.5f, 0.5f, -0.5f, colour2, new Vector2f(0, 0), new Vector3f(0, 1, 0)),			//4
				new Vertex(0.5f, 0.5f, -0.5f, colour2, new Vector2f(1, 0), new Vector3f(0, 1, 0)),			//5
				new Vertex(0.5f, 0.5f, 0.5f, colour2, new Vector2f(1, 1), new Vector3f(0, 1, 0)),			//6
				new Vertex(-0.5f, 0.5f, 0.5f, colour2, new Vector2f(0, 1), new Vector3f(0, 1, 0)),			//7
				
				//more vertices needed so that each can have it's own normal
				//left face
				new Vertex(-0.5f, 0.5f, -0.5f, colour2, new Vector2f(0, 0), new Vector3f(-1, 0, 0)),		//8 (copy of 4 with a different normal)
				new Vertex(-0.5f, -0.5f, 0.5f, colour1, new Vector2f(0, 1), new Vector3f(-1, 0, 0)),		//9 (copy of 3 with a different normal)
				new Vertex(-0.5f, 0.5f, 0.5f, colour2, new Vector2f(0, 1), new Vector3f(-1, 0, 0)),			//10 (copy of 7 with a different normal)
				new Vertex(-0.5f, -0.5f, -0.5f, colour1, new Vector2f(0, 0), new Vector3f(-1, 0, 0)),		//11 (copy of 0 with a different normal)
				
				//right face
				new Vertex(0.5f, 0.5f, 0.5f, colour2, new Vector2f(1, 1), new Vector3f(1, 0, 0)),			//12 (copy of 6 with a different normal)
				new Vertex(0.5f, -0.5f, -0.5f, colour1, new Vector2f(1, 0), new Vector3f(1, 0, 0)),			//13 (copy of 1 with a different normal)
				new Vertex(0.5f, 0.5f, -0.5f, colour2, new Vector2f(1, 0), new Vector3f(1, 0, 0)),			//14 (copy of 5 with a different normal)
				new Vertex(0.5f, -0.5f, 0.5f, colour1, new Vector2f(1, 1), new Vector3f(1, 0, 0)),			//15 (copy of 2 with a different normal)
				
				//front face
				new Vertex(-0.5f, 0.5f, 0.5f, colour2, new Vector2f(0, 1), new Vector3f(0, 0, 1)),			//16 (copy of 7 with a different normal)
				new Vertex(-0.5f, -0.5f, 0.5f, colour1, new Vector2f(0, 1), new Vector3f(0, 0, 1)),			//17 (copy of 3 with a different normal)
				new Vertex(0.5f, 0.5f, 0.5f, colour2, new Vector2f(1, 1), new Vector3f(0, 0, 1)),			//18 (copy of 6 with a different normal)
				new Vertex(0.5f, -0.5f, 0.5f, colour1, new Vector2f(1, 1), new Vector3f(0, 0, 1)),			//19 (copy of 2 with a different normal)
				
				//back face
				new Vertex(0.5f, 0.5f, -0.5f, colour2, new Vector2f(1, 0), new Vector3f(0, 0, -1)),			//20 (copy of 5 with a different normal)
				new Vertex(0.5f, -0.5f, -0.5f, colour1, new Vector2f(1, 0), new Vector3f(0, 0, -1)),		//21 (copy of 1 with a different normal)
				new Vertex(-0.5f, 0.5f, -0.5f, colour2, new Vector2f(0, 0), new Vector3f(0, 0, -1)),		//22 (copy of 4 with a different normal)
				new Vertex(-0.5f, -0.5f, -0.5f, colour1, new Vector2f(0, 0), new Vector3f(0, 0, -1)),		//23 (copy of 0 with a different normal)
		});
		addTriangle(0, 1, 3); //bottom face
		addTriangle(3, 1, 2); //bottom face

		addTriangle(7, 6, 4); //top face
		addTriangle(4, 6, 5); //top face

		addTriangle(8, 9, 10); //left face
		addTriangle(11, 9, 8); //left face

		addTriangle(12, 13, 14); //right face
		addTriangle(15, 13, 12); //right face

		addTriangle(16, 17, 18); //front face
		addTriangle(18, 17, 19); //front face

		addTriangle(20, 21, 22); //back face
		addTriangle(22, 21, 23); //back face
	}

}
