package graphics.object.model;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Arrays;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import graphics.scene.RenderModel;
import graphics.scene.RenderScene;
import graphics.shaders.ShaderProgram;
import graphics.texture.Texture;
import math.matrix.Matrix4f;
import math.vector.Vector3f;

public class Model {
	
	private boolean modified = false;//if the model has been modified it gets reuploaded to the GPU
	private boolean destroyed = false;
	private Texture texture;
	private ArrayList<Vertex> vertices;//vertex "array"
	private ArrayList<int[]> triangles;
	protected Matrix4f modelMatrix;
	private Matrix4f biasDepthMVP = null;//bias depth mvp saved on last shadow mapping
	private FloatBuffer matrix44Buffer;
	private FloatBuffer vertexBuffer;//for storing and uploading vertex data
	private IntBuffer indexBuffer;//for storing and uploading indices
	
	//OpenGL objects
	private int VAO;//vertex array object
	private int VBO;//vertex buffer object
	private int IBO;//index buffer object
	
	public Model(){
		this(null);
	}
	
	public Model(Texture texture){
		this.texture = texture;
		vertices = new ArrayList<Vertex>();
		triangles = new ArrayList<int[]>();
		modelMatrix = new Matrix4f();
		matrix44Buffer = BufferUtils.createFloatBuffer(16);
		VAO = GL30.glGenVertexArrays();
		GL30.glBindVertexArray(VAO);
		VBO = GL15.glGenBuffers();
		GL30.glBindVertexArray(0);
		IBO = GL15.glGenBuffers();
	}
	
	public int addVertex(Vertex v){//returns vertex index
		vertices.add(v);
		modified = true;
		return vertices.size() - 1;
	}
	
	public void addVertices(Vertex[] v){
		vertices.addAll(Arrays.asList(v));
		modified = true;
	}
	
	public void removeVertex(int i){
		vertices.remove(i);
		modified = true;
	}
	
	public int getVerticesCount(){
		return vertices.size();
	}
	
	public void addTriangle(int a, int b, int c){
		triangles.add(new int[]{a,b,c});
		modified = true;
	}
	
	public void removeTriangle(int i){
		triangles.remove(i);
		modified = true;
	}
	
	protected void setupMatrix(Vector3f position, Vector3f preRotationScale, Vector3f rotation, Vector3f scale){
		modelMatrix.setIdentity();
		modelMatrix.translate(position);
		modelMatrix.scale(preRotationScale);
		modelMatrix.rotate((float)Math.toRadians(rotation.x), (float)Math.toRadians(rotation.y), (float)Math.toRadians(rotation.z));
		modelMatrix.scale(scale);
	}
	
	public void addToScene(RenderScene scene, ShaderProgram shaders, Vector3f position, Vector3f rotation, Vector3f scale) {//move to the new position/rotation and then render
		addToScene(scene, shaders, position, Vector3f.ONES, rotation, scale);
	}
	
	public void addToScene(RenderScene scene, ShaderProgram shaders, Vector3f position, Vector3f preRotationScale, Vector3f rotation, Vector3f scale) {//move to the new position/rotation and then render
		if(destroyed){
			System.err.print("TRYING TO RENDER A DESTROYED MODEL!");
			return;
		}
		//upload the model matrix to the shaders
		if(position != null && rotation != null)
			setupMatrix(position, preRotationScale, rotation, scale);
		//if the model has been modified, reupload it to the GPU
		if(modified){
			upload();
			modified = false;
		}
		scene.add(new RenderModel(this, shaders));
	}
	
	public Matrix4f render(ShaderProgram shaders, Matrix4f viewMatrix, boolean mapping){//NB! Should not be used anywhere but in RenderModel class
		//returns model-view matrix
		Matrix4f modelViewMatrix = modelMatrix.multipliedByALeftMatrix(viewMatrix);
		modelViewMatrix.store(matrix44Buffer);
		matrix44Buffer.flip();
		GL20.glUniformMatrix4fv(shaders.getModelViewMatrixLocation(), false, matrix44Buffer);
		
		if(!mapping){//not when mapping shadows
			if(biasDepthMVP != null){
				biasDepthMVP.store(matrix44Buffer);
				matrix44Buffer.flip();
				biasDepthMVP = null;
				GL20.glUniformMatrix4fv(shaders.getBiasDepthMVPLocation(), false, matrix44Buffer);
			}
			Matrix4f normalMatrix = modelViewMatrix.invert();//transposed when uploaded
			normalMatrix.store(matrix44Buffer);
			matrix44Buffer.flip();
			GL20.glUniformMatrix4fv(shaders.getNormalMatrixLocation(), true, matrix44Buffer);
	        GL20.glUniform1i(shaders.getUseTextureUniformLocation(), texture != null ? GL11.GL_TRUE : GL11.GL_FALSE);
		}
		
		GL30.glBindVertexArray(VAO);
		GL20.glEnableVertexAttribArray(0);//position
		GL20.glEnableVertexAttribArray(1);//colour
		GL20.glEnableVertexAttribArray(2);//uv
		GL20.glEnableVertexAttribArray(3);//normal
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, IBO);
        if(texture != null){
        	texture.bind(0);
        } else 
            Texture.unbind(0);
        
        GL11.glDrawElements(GL11.GL_TRIANGLES, triangles.size() * 3, GL11.GL_UNSIGNED_INT, 0);
        Texture.unbind(0);
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
        return modelViewMatrix;
	}

	private void upload() {
		//storing data to buffers
		vertexBuffer = BufferUtils.createFloatBuffer(vertices.size() * Vertex.SIZE);
		for(Vertex v : vertices)
			vertexBuffer.put(v.getData());
		vertexBuffer.flip();
		indexBuffer = BufferUtils.createIntBuffer(triangles.size() * 3);
		for(int[] t : triangles)
			indexBuffer.put(t);
		indexBuffer.flip();
		GL30.glBindVertexArray(VAO);
		//uploading vertex data to VBO
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, VBO);
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, vertexBuffer, GL15.GL_STREAM_DRAW);
		GL20.glVertexAttribPointer(0, 3, GL11.GL_FLOAT, false, Vertex.STRIDE, 0);//position
		GL20.glVertexAttribPointer(1, 4, GL11.GL_FLOAT, false, Vertex.STRIDE, 12);//colour
		GL20.glVertexAttribPointer(2, 2, GL11.GL_FLOAT, false, Vertex.STRIDE, 28);//uv
		GL20.glVertexAttribPointer(3, 3, GL11.GL_FLOAT, false, Vertex.STRIDE, 36);//normal
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
		GL30.glBindVertexArray(0);
		//uploading indices to IBO
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, IBO);
		GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, indexBuffer, GL15.GL_STATIC_DRAW);
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
	}
	
	public void destroyModel(){
		GL30.glBindVertexArray(VAO);
		GL15.glDeleteBuffers(VBO);
		GL30.glBindVertexArray(0);
		GL30.glDeleteVertexArrays(VAO);
		GL15.glDeleteBuffers(IBO);
		vertices.clear();
		triangles.clear();
		destroyed = true;
	}
	
	public Matrix4f getModelMatrix(){
		return modelMatrix;
	}
	
	public FloatBuffer getMatrixBuffer(){
		return matrix44Buffer;
	}
	
	public Texture getTexture(){
		return texture;
	}

	public void saveBiasDepthMVP(Matrix4f biasDepthMVP){
		this.biasDepthMVP = biasDepthMVP;
	}
	
	public void modify(){
		modified = true;
	}

}
