package graphics.object.model;

import graphics.texture.Texture;
import math.vector.Vector2f;
import math.vector.Vector3f;
import math.vector.Vector4f;

public class Cilinder extends Model {
	
	public Cilinder(Texture texture, int sectors, Vector4f colour1, Vector4f colour2, boolean sealedTop, boolean sealedBottom){
		super(texture);
		int circleVertices = sectors + 1;//one vertex is double but with different UV values
		float degrees360 = (float) (2.0 * Math.PI);
		float sector = degrees360 / sectors;
		for(int i = 0; i < circleVertices; i++){
			float yaw = sector * i;
			float x = (float) Math.sin(yaw);
			float z = (float) Math.cos(yaw);
			addVertex(new Vertex(x, 0.5f, z, colour1, new Vector2f(yaw / degrees360, 1), new Vector3f(x, 0, z)));
			addVertex(new Vertex(x, -0.5f, z, colour2, new Vector2f(yaw / degrees360, 0), new Vector3f(x, 0, z)));
		}
		int top = addVertex(new Vertex(0, 0.5f, 0, colour1, new Vector2f(0, 0), new Vector3f(0, 1, 0)));
		int bottom = addVertex(new Vertex(0, -0.5f, 0, colour2, new Vector2f(0, 0), new Vector3f(0, -1, 0)));
	
		for(int i = 0; i < circleVertices - 1; i++){//excludes the 2 extra seam vertices
			int index = i * 2;
			addTriangle(index, index + 1, index + 2);//side first triangle
			addTriangle(index + 2, index + 1, index + 3);//side second triangle
			if(sealedTop)
				addTriangle(top, index, index + 2);//top triangle
			if(sealedBottom)
				addTriangle(bottom, index + 3, index + 1);//bottom triangle
		}
	}

}
