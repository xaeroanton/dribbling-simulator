package graphics.object.model;

import graphics.texture.Texture;
import math.vector.Vector2f;
import math.vector.Vector3f;
import math.vector.Vector4f;

public class Sphere extends Model {
	
	public Sphere(int yawSectors, int pitchSectors){
		this(null, yawSectors, pitchSectors);
	}

	public Sphere(Texture texture, int yawSectors, int pitchSectors){//looks the best if 2 * pitchSectors == yawSectors
		super(texture);
		//adding vertices
		int yawVertices = yawSectors + 1;//one extra vertice for correct UV
		double degrees90 = Math.PI / 2.0;
		//yaw sectors go from 0 to 360 degrees
		//pitch sectors go from -90 to 90 degrees
		double yawSectorSize = Math.PI * 2.0 / yawSectors; //Math.PI * 2 is 360 degrees
		double pitchSectorSize = Math.PI / pitchSectors;
		for(int i = 0; i < pitchSectors + 1; i++){
			double pitch = -degrees90 + i * pitchSectorSize;
			double cosPitch = Math.cos(pitch);
			float Y = (float) Math.sin(pitch);//Y of the scaled circle
			//float V = (float) Math.sin(pitch) / 2.0f;
			float V = (float)(-(pitch + degrees90) / (2*degrees90));
			for(int j = 0; j < yawVertices; j++){
				double yaw = j * yawSectorSize;
				//calculating x, z coordinates that go in a circle counter-clockwise not counting pitch rotation
				double circleX = Math.sin(yaw);
				double circleZ = Math.cos(yaw);
				//scaling the circle depending on the pitch rotation
				float X = (float) (circleX * cosPitch);
				float Z = (float) (circleZ * cosPitch);
				//float U = (float) Math.sin(yaw / 4.0);
				float U = (float)(yaw / (4*degrees90));
				addVertex(new Vertex(X, Y, Z, new Vector4f(1.0f, 1.0f, 1.0f, 1.0f), new Vector2f(U, V)/*new Vector2f((X + 1.0f)/2.0f, (Y - 1.0f)/-2.0f)*/, new Vector3f(X, Y, Z)));
			}
		}
		//connecting vertices
		for(int i = 1; i < pitchSectors + 1; i++){
			int offset = i * yawVertices;
			for(int j = 0; j < yawSectors; j++){
				if(i == 1){
					addTriangle(offset + j, j, offset + (j + 1));//triangle connected to the correct bottom vertex
				} else if(i == pitchSectors){
					addTriangle(offset + j, offset + j - yawVertices, offset + (j + 1) - yawVertices);
				} else {
					int nextVertex = offset + (j + 1) % yawVertices;
					addTriangle(offset + j, offset + j - yawVertices, nextVertex);
					addTriangle(nextVertex, offset + j - yawVertices, nextVertex - yawVertices);
				}
			}
		}
	}

}
