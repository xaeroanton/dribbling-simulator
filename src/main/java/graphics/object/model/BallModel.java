package graphics.object.model;

import graphics.texture.Texture;
import math.matrix.Matrix4f;
import math.vector.Vector3f;

public class BallModel extends Sphere {
	private Matrix4f totalRotationMatrix;

	public BallModel(Texture texture, int yawSectors, int pitchSectors) {
		super(texture, yawSectors, pitchSectors);
		totalRotationMatrix = new Matrix4f();
	}
	
	public void rotate(Vector3f angularVelocity, float time){
		Matrix4f rotationMatrix = new Matrix4f();
		Vector3f rotationAxis = angularVelocity.normalize();
		rotationMatrix.rotate((float)Math.toRadians(angularVelocity.length() * time), rotationAxis.x, rotationAxis.y, rotationAxis.z);
		totalRotationMatrix = totalRotationMatrix.multipliedByALeftMatrix(rotationMatrix);
	}
	
	protected void setupMatrix(Vector3f position, Vector3f preRotationScale, Vector3f rotation, Vector3f scale){
		modelMatrix.setIdentity();
		modelMatrix.translate(position);
		modelMatrix.scale(preRotationScale);
		modelMatrix = totalRotationMatrix.multipliedByALeftMatrix(modelMatrix);
		modelMatrix.scale(scale);
	}

}
