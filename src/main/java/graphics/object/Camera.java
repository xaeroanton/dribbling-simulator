package graphics.object;

import math.matrix.Matrix4f;
import math.matrix.ProjectionMatrix;
import math.object.SmoothSpaceObject;
import math.vector.Vector3f;

public class Camera extends SmoothSpaceObject {
	
	private ProjectionMatrix projectionMatrix;
	protected Matrix4f viewMatrix;
	protected Matrix4f directionMatrix;//for directional light
	protected boolean viewMatrixUsable;
	
	public Camera(ProjectionMatrix projectionMatrix){
		position = new Vector3f(0, 0, 0);
		rotation = new Vector3f(0, 0, 0);
		this.projectionMatrix = projectionMatrix;
		viewMatrix = new Matrix4f();
		viewMatrixUsable = false;
		directionMatrix = new Matrix4f();
	}

	public Matrix4f getDirectionMatrix() {
		return directionMatrix;
	}

	public void updateView(float partialTicks){
		if(!viewMatrixUsable || partialTicks != -1){
			directionMatrix.setIdentity();
			directionMatrix.rotate((float)Math.toRadians(-rotation.x), (float)Math.toRadians(-rotation.y), (float)Math.toRadians(-rotation.z));
			
			viewMatrix = new Matrix4f(directionMatrix);
			Vector3f smoothPosition = getPosition(partialTicks);
			viewMatrix.translate(-smoothPosition.x, -smoothPosition.y, -smoothPosition.z);
			viewMatrixUsable = true;
		}
	}
	
	public ProjectionMatrix getProjectionMatrix(){
		return projectionMatrix;
	}
	
	public Matrix4f getViewMatrix(){
		return viewMatrix;
	}
	
	public Vector3f getLookVector(){//normalized vector pointing in the direction the camera is looking
		float sinX = (float) Math.sin(Math.toRadians(rotation.x));
		float cosX = (float) Math.cos(Math.toRadians(rotation.x));
		float sinY = (float) Math.sin(Math.toRadians(-rotation.y));
		float cosY = (float) Math.cos(Math.toRadians(-rotation.y));
		return new Vector3f(sinY * cosX, sinX, -cosY * cosX);
	}
	
	public Vector3f getLeftVector(){//normalized vector pointing to the left
		float sinY = (float) Math.sin(Math.toRadians(-rotation.y));
		float cosY = (float) Math.cos(Math.toRadians(-rotation.y));
		return new Vector3f(-cosY, 0, -sinY);
	}
	
	public Vector3f getRightVector(){//normalized vector pointing to the right
		return getLeftVector().negative();
	}
	
	public void move(float moveSpeed){
		this.addToPosition(0, 0, moveSpeed);
	}
	
}
