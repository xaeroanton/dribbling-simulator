package graphics.object;

import java.io.IOException;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL32;

import graphics.exception.GLException;
import graphics.exception.ShaderException;
import graphics.scene.RenderScene;
import graphics.shaders.ShaderProgram;
import graphics.texture.FrameBuffer;
import graphics.texture.Texture;
import math.matrix.Matrix4f;
import math.matrix.ProjectionMatrix;
import math.vector.Vector3f;

public class ShadowMapper extends Camera {
	
	private ShaderProgram depthShader;
	private Texture texture;
	private FrameBuffer frameBuffer;
	private Vector3f direction;
	private Vector3f savedDirection;

	public ShadowMapper(String shaderName, int width, int height, Vector3f position, Vector3f direction, ProjectionMatrix projectionMatrix) throws GLException, IOException, ShaderException{
		super(projectionMatrix);
		this.position = new Vector3f(position);
		this.direction = new Vector3f(direction);
		texture = new Texture(width, height);
		frameBuffer = new FrameBuffer(texture){
			@Override
			public void init() throws GLException {
				bind();
				texture.bind(1);
				GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_DEPTH_COMPONENT, texture.getWidth(), texture.getHeight(), 0, GL11.GL_DEPTH_COMPONENT, GL11.GL_FLOAT, 0);
				GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
				GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);
				GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
				GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
				GL32.glFramebufferTexture(GL30.GL_FRAMEBUFFER, GL30.GL_DEPTH_ATTACHMENT, texture.get(), 0);
				GL11.glDrawBuffer(GL11.GL_NONE);
				if(GL30.glCheckFramebufferStatus(GL30.GL_FRAMEBUFFER) != GL30.GL_FRAMEBUFFER_COMPLETE)
					throw new GLException("Error initializing shadow map frame buffer: " + frameBuffer.getLocation() + " " + texture.get());
				Texture.unbind(1);
				FrameBuffer.unbind();
			}
		};
		frameBuffer.init();
		depthShader = new ShaderProgram(shaderName);
	}
	
	@Override
	public void saveState(){
		super.saveState();
		savedDirection = new Vector3f(direction.x, direction.y, direction.z);
	}
	
	public Vector3f getDirection(){
		return direction;
	}
	
	public Vector3f getDirection(float partialTicks){
		return new Vector3f(
				savedDirection.x + (direction.x - savedDirection.x) * partialTicks, 
				savedDirection.y + (direction.y - savedDirection.y) * partialTicks, 
				savedDirection.z + (direction.z - savedDirection.z) * partialTicks);
		
	}
	
	@Override
	public void updateView(float partialTicks){
		if(!viewMatrixUsable || partialTicks != -1){
			Vector3f smoothPosition = getPosition(partialTicks);
			Vector3f smoothDirection = getDirection(partialTicks);
			viewMatrix = Matrix4f.lookAt(smoothPosition, smoothPosition.sum(smoothDirection), new Vector3f(0, 1, 0));
			viewMatrixUsable = true;
		}
	}
	
	public void bindShadowMap(ShaderProgram shaders){
		texture.bind(1);
		GL20.glUniform1i(shaders.getShadowMapWidthLocation(), texture.getWidth());
		GL20.glUniform1i(shaders.getShadowMapHeightLocation(), texture.getHeight());
	}
	
	public void unload(){
		depthShader.unloadProgram();
	}

	public void mapShadows(RenderScene scene) {
		frameBuffer.bind();
		GL11.glViewport(0, 0, texture.getWidth(), texture.getHeight());
		GL11.glClear(GL11.GL_DEPTH_BUFFER_BIT);
		scene.renderScene(this, depthShader, null, true);
		FrameBuffer.unbind();
	}

}
