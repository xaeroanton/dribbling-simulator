package math.vector;

import math.matrix.Matrix4f;

public class Vector3f extends Vector2f {
	
	public static final Vector3f ZERO = new Vector3f(0,0,0);
	public static final Vector3f ONES = new Vector3f(1,1,1);
	
	private static final Vector3f standartBasisI = new Vector3f(1, 0, 0);
	private static final Vector3f standartBasisJ = new Vector3f(0, -1, 0);
	private static final Vector3f standartBasisK = new Vector3f(0, 0, 1);
	
	public float z;
	
	public Vector3f(float x, float y, float z){
		super(x, y);
		this.z = z;
	}
	
	public Vector3f(Vector3f copyFrom){
		this(copyFrom.x, copyFrom.y, copyFrom.z);
	}
	
	public float[] getXYZ(){
		return getXYZ(z);
	}

	public float[] getXYZW(float w){
		return new float[] {x, y, z, w};
	}

	public float[] getRGB(){
		return getXYZ();
	}

	public float[] getRGBA(float a){
		return getXYZW(a);
	}

	public float[] getRGBA() {
		return getRGBA(1);
	}
	
	public Vector3f multiplyByAMatrix(Matrix4f matrix){
		Vector3f result = new Vector3f(x, y, z);
		result.x = x * matrix.m00 + y * matrix.m10 + z * matrix.m20 + 1 * matrix.m30;
		result.y = x * matrix.m01 + y * matrix.m11 + z * matrix.m21 + 1 * matrix.m31;
		result.z = x * matrix.m02 + y * matrix.m12 + z * matrix.m22 + 1 * matrix.m32;
		return result;
	}

	@Override
	public Vector3f negative(){
		return new Vector3f(-x, -y, -z);
	}
	
	public Vector3f sum(Vector3f b){
		return new Vector3f(x + b.x, y + b.y, z + b.z);
	}
	
	public Vector3f multiplication(Vector3f b){
		return new Vector3f(x * b.x, y * b.y, z * b.z);
	}
	
	public Vector3f square(){
		return new Vector3f(x*x, y*y, z*z);
	}
	
	public static Vector3f cross(Vector3f a, Vector3f b){
		Vector3f c1 = standartBasisI.scale(a.y * b.z - a.z * b.y);
		Vector3f c2 = standartBasisJ.scale(a.z * b.x - a.x * b.z).negative();
		Vector3f c3 = standartBasisK.scale(a.x * b.y - a.y * b.x);
		return c1.sum(c2).sum(c3);
	}
	
	@Override
	public Vector3f scale(float s){
		return new Vector3f(x * s, y * s, z * s);
	}
	
	public String toString(){
		return "( " + x + ", " + y + ", " + z + ")";
	}
	
	public float length(){
		return (float) Math.sqrt(x * x + y * y + z * z);
	}

	public Vector3f normalize() {
		float l = length();
		if(l == 0)
			return new Vector3f(0, 0, 0);
		return new Vector3f(x / l, y / l, z / l);
	}
	
	public boolean equals(Vector3f other){
		return other != null && other.x == x && other.y == y && other.z == z;
	}

}
