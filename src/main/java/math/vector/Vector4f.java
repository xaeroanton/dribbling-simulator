package math.vector;

public class Vector4f extends Vector3f {

	public float w;
	
	public Vector4f(float x, float y, float z, float w){
		super(x, y, z);
		this.w = w;
	}

	public float[] getXYZW(){
		return getXYZW(w);
	}

	@Override
	public float[] getRGBA(){
		return getXYZW();
	}
	
	@Override
	public Vector4f negative(){
		return new Vector4f(-x, -y, -z, -w);
	}
	
	@Override
	public Vector4f scale(float s){
		return new Vector4f(x * s, y * s, z * s, w * s);
	}

	public String toString(){
		return "( " + x + ", " + y + ", " + z + ", " + w + ")";
	}

}
