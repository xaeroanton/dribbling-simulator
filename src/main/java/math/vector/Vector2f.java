package math.vector;

public class Vector2f {
	
	public float x;
	public float y;
	
	public Vector2f(float x, float y){
		this.x = x;
		this.y = y;
	}

	public float[] getXY() {
		return new float[] {x, y};
	}

	public float[] getXYZ(float z) {
		return new float[] {x, y, z};
	}

	public Vector2f negative() {
		return new Vector2f(-x, -y);
	}

	public Vector2f scale(float s) {
		return new Vector2f(x * s, y * s);
	}

}
