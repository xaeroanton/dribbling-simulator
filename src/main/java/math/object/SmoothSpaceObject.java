package math.object;

import math.vector.Vector3f;

/*
 * Some object in the 3D space that takes partial ticks in count
 */

public class SmoothSpaceObject extends SpaceObject {
	
	private Vector3f savedPosition;
	private Vector3f savedRotation;
	private Vector3f savedScale;
	
	public SmoothSpaceObject(){
		super();
		savedPosition = position;
		savedRotation = rotation;
		savedScale = scale;
	}
	
	public void saveState(){
		savedPosition = new Vector3f(position.x, position.y, position.z);
		savedRotation = new Vector3f(rotation.x, rotation.y, rotation.z);
		savedScale = new Vector3f(scale.x, scale.y, scale.z);
	}
	
	public Vector3f getPosition(float partialTicks){
		return new Vector3f(
				savedPosition.x + (position.x - savedPosition.x) * partialTicks, 
				savedPosition.y + (position.y - savedPosition.y) * partialTicks, 
				savedPosition.z + (position.z - savedPosition.z) * partialTicks);
	}
	
	public Vector3f getRotation(float partialTicks){
		return new Vector3f(
				savedRotation.x + (rotation.x - savedRotation.x) * partialTicks, 
				savedRotation.y + (rotation.y - savedRotation.y) * partialTicks, 
				savedRotation.z + (rotation.z - savedRotation.z) * partialTicks);
		
	}
	
	public Vector3f getScale(float partialTicks){
		return new Vector3f(
				savedScale.x + (scale.x - savedScale.x) * partialTicks, 
				savedScale.y + (scale.y - savedScale.y) * partialTicks, 
				savedScale.z + (scale.z - savedScale.z) * partialTicks);
		
	}
}
