package math.object;

import math.vector.Vector3f;

/*
 * An object in the 3D space that takes partial ticks in count and has a linearVelocity
 */

public class SpacePhysicalObject extends SmoothSpaceObject {
	
	protected Vector3f linearVelocity;//units/second
	protected Vector3f angularVelocity;//degrees/second
	
	public SpacePhysicalObject(){
		super();
		linearVelocity = new Vector3f(0, 0, 0);
		angularVelocity = new Vector3f(0, 0, 0);
	}
	
	public Vector3f getLinearVelocity(){
		return new Vector3f(linearVelocity);
	}
	
	public void setLinearVelocity(Vector3f v){
		setLinearVelocity(v.x, v.y, v.z);
	}
	
	public void setLinearVelocity(float x, float y, float z){
		linearVelocity.x = x;
		linearVelocity.y = y;
		linearVelocity.z = z;
	}
	
	public void addLinearVelocity(Vector3f v){
		addLinearVelocity(v.x, v.y, v.z);
	}
	
	public void addLinearVelocity(float x, float y, float z){
		linearVelocity.x += x;
		linearVelocity.y += y;
		linearVelocity.z += z;
	}
	
	public Vector3f getAngularVelocity(){
		return new Vector3f(angularVelocity);
	}
	
	public void setAngularVelocity(Vector3f v){
		setAngularVelocity(v.x, v.y, v.z);
	}
	
	public void setAngularVelocity(float x, float y, float z){
		angularVelocity.x = x;
		angularVelocity.y = y;
		angularVelocity.z = z;
	}
	
	public void addAngularVelocity(Vector3f v){
		addAngularVelocity(v.x, v.y, v.z);
	}
	
	public void addAngularVelocity(float x, float y, float z){
		angularVelocity.x += x;
		angularVelocity.y += y;
		angularVelocity.z += z;
	}
	
	public void update(float time){
		addToPosition(linearVelocity.scale(time));
		addToRotation(angularVelocity.scale(time));
	}

}
