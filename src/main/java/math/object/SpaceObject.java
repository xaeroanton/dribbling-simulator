package math.object;

import math.vector.Vector3f;

/*
 * Some object in the 3D space
 * For example: camera, basketball
 */

public class SpaceObject {
	
	protected Vector3f position;
	protected Vector3f rotation;
	protected Vector3f scale;
	
	public SpaceObject(){
		position = new Vector3f(0,0,0);
		rotation = new Vector3f(0,0,0);
		scale = new Vector3f(1,1,1);
	}
	
	public void setPosition(Vector3f vec){
		setPosition(vec.x, vec.y, vec.z);
	}
	
	public void setPosition(float x, float y, float z){
		position.x = x;
		position.y = y;
		position.z = z;
	}
	
	public void addToPosition(Vector3f vec){
		addToPosition(vec.x, vec.y, vec.z);
	}
	
	public void addToPosition(float x, float y, float z){
		position.x += x;
		position.y += y;
		position.z += z;
	}
	
	public void setRotation(Vector3f vec){
		setRotation(vec.x, vec.y, vec.z);
	}
	
	public void setRotation(float x, float y, float z){
		rotation.x = x;
		rotation.y = y;
		rotation.z = z;
	}
	
	public void addToRotation(Vector3f vec){
		addToRotation(vec.x, vec.y, vec.z);
	}
	
	public void addToRotation(float x, float y, float z){
		rotation.x += x;
		rotation.y += y;
		rotation.z += z;
	}
	
	public Vector3f getPosition(){
		return new Vector3f(position);
	}
	
	public Vector3f getRotation(){
		return new Vector3f(rotation);
	}
	
	public Vector3f getScale(){
		return new Vector3f(scale);
	}
	
	public void setScale(float x, float y, float z){
		scale.x = x;
		scale.y = y;
		scale.z = z;
	}
	
}
