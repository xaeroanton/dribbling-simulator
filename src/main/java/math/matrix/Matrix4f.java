package math.matrix;

import java.nio.FloatBuffer;

import org.lwjgl.opengl.GL20;

import math.vector.Vector3f;

public class Matrix4f {

	//column number, row number (reverse from how it is in math)
	public float m00, m01, m02, m03, m10, m11, m12, m13, m20, m21, m22, m23, m30, m31, m32, m33;
	
	public Matrix4f(){
		setIdentity();
	}
	
	public Matrix4f(Matrix4f copyFrom){
		m00 = copyFrom.m00;
		m01 = copyFrom.m01;
		m02 = copyFrom.m02;
		m03 = copyFrom.m03;
		m10 = copyFrom.m10;
		m11 = copyFrom.m11;
		m12 = copyFrom.m12;
		m13 = copyFrom.m13;
		m20 = copyFrom.m20;
		m21 = copyFrom.m21;
		m22 = copyFrom.m22;
		m23 = copyFrom.m23;
		m30 = copyFrom.m30;
		m31 = copyFrom.m31;
		m32 = copyFrom.m32;
		m33 = copyFrom.m33;
	}
	
	public static Matrix4f lookAt(Vector3f eyePosition, Vector3f lookPoint, Vector3f upVector){
		Matrix4f M = new Matrix4f();
		Vector3f lookVector = new Vector3f(lookPoint.x - eyePosition.x, lookPoint.y - eyePosition.y, lookPoint.z - eyePosition.z);
		//normalizing
		lookVector = lookVector.normalize();
		Vector3f normalizedUp = upVector.normalize();
		//calculating
		Vector3f s = Vector3f.cross(lookVector, normalizedUp);
		Vector3f u = Vector3f.cross(s, lookVector);
		//constructing
		M.m00 = s.x;
		M.m10 = s.y;
		M.m20 = s.z;
		M.m01 = u.x;
		M.m11 = u.y;
		M.m21 = u.z;
		M.m02 = -lookVector.x;
		M.m12 = -lookVector.y;
		M.m22 = -lookVector.z;
		//transforming
		Matrix4f transformMatrix = new Matrix4f();
		transformMatrix.m30 = -eyePosition.x;
		transformMatrix.m31 = -eyePosition.y;
		transformMatrix.m32 = -eyePosition.z;
		return transformMatrix.multipliedByALeftMatrix(M);
	}
	
	public void upload(FloatBuffer matrix44Buffer, int location){
		//we don't upload the view matrix yet, we will upload the model view matrix instead
		//uploading the matrix as uniforms to the GPU
		store(matrix44Buffer);
		matrix44Buffer.flip();
		GL20.glUniformMatrix4fv(location, false, matrix44Buffer);
	}
	
	public void store(FloatBuffer buffer){
		buffer.put(m00);
		buffer.put(m01);
		buffer.put(m02);
		buffer.put(m03);
		buffer.put(m10);
		buffer.put(m11);
		buffer.put(m12);
		buffer.put(m13);
		buffer.put(m20);
		buffer.put(m21);
		buffer.put(m22);
		buffer.put(m23);
		buffer.put(m30);
		buffer.put(m31);
		buffer.put(m32);
		buffer.put(m33);
	}	
	
	public String toString() {
		StringBuilder buf = new StringBuilder();
		buf.append(m00).append(' ').append(m10).append(' ').append(m20).append(' ').append(m30).append('\n');
		buf.append(m01).append(' ').append(m11).append(' ').append(m21).append(' ').append(m31).append('\n');
		buf.append(m02).append(' ').append(m12).append(' ').append(m22).append(' ').append(m32).append('\n');
		buf.append(m03).append(' ').append(m13).append(' ').append(m23).append(' ').append(m33).append('\n');
		return buf.toString();
	}
	
	public void setIdentity() {
		m00 = 1.0f;
		m01 = 0.0f;
		m02 = 0.0f;
		m03 = 0.0f;
		m10 = 0.0f;
		m11 = 1.0f;
		m12 = 0.0f;
		m13 = 0.0f;
		m20 = 0.0f;
		m21 = 0.0f;
		m22 = 1.0f;
		m23 = 0.0f;
		m30 = 0.0f;
		m31 = 0.0f;
		m32 = 0.0f;
		m33 = 1.0f;
	}
	
	public void translate(Vector3f vec){
		translate(vec.x, vec.y, vec.z);
	}
	
	public void translate(float x, float y, float z){
		m30 += m00 * x + m10 * y + m20 * z;//add to current x but rotated using the rotation 3x3 sub-matrix
		m31 += m01 * x + m11 * y + m21 * z;
		m32 += m02 * x + m12 * y + m22 * z;
		m33 += m03 * x + m13 * y + m23 * z;
	}

	public void scale(Vector3f vec){
		scale(vec.x, vec.y,vec.z);
	}
	
	public void scale(float x, float y, float z){
		//multiply the rotation 3x3 sub-matrix elements based on element's first coordinate
		m00 *= x;
		m01 *= x;
		m02 *= x;
		m03 *= x;
		m10 *= y;
		m11 *= y;
		m12 *= y;
		m13 *= y;
		m20 *= z;
		m21 *= z;
		m22 *= z;
		m23 *= z;
	}
	
	public void rotate(float angle, Vector3f axis) {
		rotate(angle, axis.x, axis.y, axis.z);
	}
	
	/*
	 * rotate() method was taken from previous versions of LWJGL and modified
	 */
	public void rotate(float angle, float x, float y, float z) {
		float c = (float) Math.cos(angle);
		float s = (float) Math.sin(angle);
		float oneminusc = 1.0f - c;
		float xy = x*y;
		float yz = y*z;
		float xz = x*z;
		float xs = x*s;
		float ys = y*s;
		float zs = z*s;

		float f00 = x*x*oneminusc+c;
		float f01 = xy*oneminusc+zs;
		float f02 = xz*oneminusc-ys;
		
		float f10 = xy*oneminusc-zs;
		float f11 = y*y*oneminusc+c;
		float f12 = yz*oneminusc+xs;
		
		float f20 = xz*oneminusc+ys;
		float f21 = yz*oneminusc-xs;
		float f22 = z*z*oneminusc+c;

		float t00 = m00 * f00 + m10 * f01 + m20 * f02;
		float t01 = m01 * f00 + m11 * f01 + m21 * f02;
		float t02 = m02 * f00 + m12 * f01 + m22 * f02;
		float t03 = m03 * f00 + m13 * f01 + m23 * f02;
		float t10 = m00 * f10 + m10 * f11 + m20 * f12;
		float t11 = m01 * f10 + m11 * f11 + m21 * f12;
		float t12 = m02 * f10 + m12 * f11 + m22 * f12;
		float t13 = m03 * f10 + m13 * f11 + m23 * f12;
		m20 = m00 * f20 + m10 * f21 + m20 * f22;
		m21 = m01 * f20 + m11 * f21 + m21 * f22;
		m22 = m02 * f20 + m12 * f21 + m22 * f22;
		m23 = m03 * f20 + m13 * f21 + m23 * f22;
		m00 = t00;
		m01 = t01;
		m02 = t02;
		m03 = t03;
		m10 = t10;
		m11 = t11;
		m12 = t12;
		m13 = t13;
	}
	
	public Matrix4f transpose(){
		return transpose(this);
	}
	
	public Matrix4f transpose(Matrix4f dest){
		if (dest == null)
			dest = new Matrix4f();
		float b00 = m00;
		float b10 = m10;
		float b20 = m20;
		float b30 = m30;
		float b01 = m01;
		float b11 = m11;
		float b21 = m21;
		float b31 = m31;
		float b02 = m02;
		float b12 = m12;
		float b22 = m22;
		float b32 = m32;
		float b03 = m03;
		float b13 = m13;
		float b23 = m23;
		float b33 = m33;
		dest.m00 = b00;
		dest.m10 = b01;
		dest.m20 = b02;
		dest.m30 = b03;
		dest.m01 = b10;
		dest.m11 = b11;
		dest.m21 = b12;
		dest.m31 = b13;
		dest.m02 = b20;
		dest.m12 = b21;
		dest.m22 = b22;
		dest.m32 = b23;
		dest.m03 = b30;
		dest.m13 = b31;
		dest.m23 = b32;
		dest.m33 = b33;
		return dest;
	}

	public void rotate(float x, float y, float z) {
		rotate(x, 1, 0, 0);
		rotate(y, 0, 1, 0);
		rotate(z, 0, 0, 1);
	}

	public void rotate(Vector3f rotation) {
		rotate(rotation.x, rotation.y, rotation.z);
	}
	
	public Matrix4f multipliedByALeftMatrix(Matrix4f left){//multiply current matrix with another matrix to the left
		Matrix4f dest = new Matrix4f();
		
		dest.m00 = left.m00 * m00 + left.m10 * m01 + left.m20 * m02 + left.m30 * m03;
		dest.m10 = left.m00 * m10 + left.m10 * m11 + left.m20 * m12 + left.m30 * m13;
		dest.m20 = left.m00 * m20 + left.m10 * m21 + left.m20 * m22 + left.m30 * m23;
		dest.m30 = left.m00 * m30 + left.m10 * m31 + left.m20 * m32 + left.m30 * m33;
		
		dest.m01 = left.m01 * m00 + left.m11 * m01 + left.m21 * m02 + left.m31 * m03;
		dest.m11 = left.m01 * m10 + left.m11 * m11 + left.m21 * m12 + left.m31 * m13;
		dest.m21 = left.m01 * m20 + left.m11 * m21 + left.m21 * m22 + left.m31 * m23;
		dest.m31 = left.m01 * m30 + left.m11 * m31 + left.m21 * m32 + left.m31 * m33;
		
		dest.m02 = left.m02 * m00 + left.m12 * m01 + left.m22 * m02 + left.m32 * m03;
		dest.m12 = left.m02 * m10 + left.m12 * m11 + left.m22 * m12 + left.m32 * m13;
		dest.m22 = left.m02 * m20 + left.m12 * m21 + left.m22 * m22 + left.m32 * m23;
		dest.m32 = left.m02 * m30 + left.m12 * m31 + left.m22 * m32 + left.m32 * m33;
		
		dest.m03 = left.m03 * m00 + left.m13 * m01 + left.m23 * m02 + left.m33 * m03;
		dest.m13 = left.m03 * m10 + left.m13 * m11 + left.m23 * m12 + left.m33 * m13;
		dest.m23 = left.m03 * m20 + left.m13 * m21 + left.m23 * m22 + left.m33 * m23;
		dest.m33 = left.m03 * m30 + left.m13 * m31 + left.m23 * m32 + left.m33 * m33;
		
		return dest;
	}	
	
	public float determinant() {
		/*
		 * m00 m01 m02 m03
		 * m10 m11 m12 m13
		 * m20 m21 m22 m23
		 * m30 m31 m32 m33
		 */
						//"back-slash" diagonals - "forward-slash" diagonals
		float f = m00 * ((m11 * m22 * m33 + m12 * m23 * m31 + m13 * m21 * m32) - (m13 * m22 * m31 + m11 * m23 * m32 + m12 * m21 * m33));
		f -= m01 * ((m10 * m22 * m33 + m12 * m23 * m30 + m13 * m20 * m32) - (m13 * m22 * m30 + m10 * m23 * m32 + m12 * m20 * m33));
		f += m02 * ((m10 * m21 * m33 + m11 * m23 * m30 + m13 * m20 * m31) - (m13 * m21 * m30 + m10 * m23 * m31 + m11 * m20 * m33));
		f -= m03 * ((m10 * m21 * m32 + m11 * m22 * m30 + m12 * m20 * m31) - (m12 * m21 * m30 + m10 * m22 * m31 + m11 * m20 * m32));
		return f;
	}
	
	public static float determinant33(float t00, float t01, float t02, float t10, float t11, float t12, float t20, float t21, float t22){
		/* t00 t10 t20
		 * t01 t11 t21
		 * t02 t12 t22
		 */
		return t00 * (t11 * t22 - t21 * t12) - t01 * (t10 * t22 - t12 * t20) + t02 * (t10 * t21 - t20 * t11);
	}
	
	public Matrix4f invert() {
		return invert(this);
	}

	public Matrix4f invert(Matrix4f dest) {
		float determinant = determinant();
		if(determinant == 0)
			return null;
		if (dest == null)
			dest = new Matrix4f();
		float t00 =  determinant33(m11, m12, m13, m21, m22, m23, m31, m32, m33);
		float t01 = -determinant33(m10, m12, m13, m20, m22, m23, m30, m32, m33);
		float t02 =  determinant33(m10, m11, m13, m20, m21, m23, m30, m31, m33);
		float t03 = -determinant33(m10, m11, m12, m20, m21, m22, m30, m31, m32);
		
		float t10 = -determinant33(m01, m02, m03, m21, m22, m23, m31, m32, m33);
		float t11 =  determinant33(m00, m02, m03, m20, m22, m23, m30, m32, m33);
		float t12 = -determinant33(m00, m01, m03, m20, m21, m23, m30, m31, m33);
		float t13 =  determinant33(m00, m01, m02, m20, m21, m22, m30, m31, m32);
		
		float t20 =  determinant33(m01, m02, m03, m11, m12, m13, m31, m32, m33);
		float t21 = -determinant33(m00, m02, m03, m10, m12, m13, m30, m32, m33);
		float t22 =  determinant33(m00, m01, m03, m10, m11, m13, m30, m31, m33);
		float t23 = -determinant33(m00, m01, m02, m10, m11, m12, m30, m31, m32);
		
		float t30 = -determinant33(m01, m02, m03, m11, m12, m13, m21, m22, m23);
		float t31 =  determinant33(m00, m02, m03, m10, m12, m13, m20, m22, m23);
		float t32 = -determinant33(m00, m01, m03, m10, m11, m13, m20, m21, m23);
		float t33 =  determinant33(m00, m01, m02, m10, m11, m12, m20, m21, m22);

		dest.m00 = t00/determinant;
		dest.m11 = t11/determinant;
		dest.m22 = t22/determinant;
		dest.m33 = t33/determinant;
		dest.m01 = t10/determinant;
		dest.m10 = t01/determinant;
		dest.m20 = t02/determinant;
		dest.m02 = t20/determinant;
		dest.m12 = t21/determinant;
		dest.m21 = t12/determinant;
		dest.m03 = t30/determinant;
		dest.m30 = t03/determinant;
		dest.m13 = t31/determinant;
		dest.m31 = t13/determinant;
		dest.m32 = t23/determinant;
		dest.m23 = t32/determinant;
		return dest;
	}	
	
}
