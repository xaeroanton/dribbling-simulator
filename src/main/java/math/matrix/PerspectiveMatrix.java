package math.matrix;

public class PerspectiveMatrix extends Matrix4f implements ProjectionMatrix {
	
	private float FOV;
	private float aspectRatio;
	private float nearPlane;
	private float farPlane;
		
	public PerspectiveMatrix(float FOV, float aspectRatio, float nearPlane, float farPlane){
		super();
		this.aspectRatio = aspectRatio;
		setFOV(FOV);
		setNearFarPlanes(nearPlane, farPlane);
		m23 = -1;
		m33 = 0;
	}
	
	public float getFOV(){
		return FOV;
	}

	public void setFOV(float FOV){
		m11 = 1.0f / (float)Math.tan(Math.toRadians(FOV / 2.0f));//y scale
		m00 = m11 / aspectRatio;//x scale
	}

	@Override
	public float getNearPlane(){
		return nearPlane;
	}

	@Override
	public float getFarPlane(){
		return farPlane;
	}

	@Override
	public void setNearFarPlanes(float near, float far){
		this.nearPlane = near;
		this.farPlane = far;
		float length = far - near;
		m22 = -2 * near;//-((farPlane + nearPlane) - frustumLength );
		m32 = -(2 * near * far / length);
	}
	
}
