package math.matrix;

import java.nio.FloatBuffer;

public interface ProjectionMatrix {
	
	public void store(FloatBuffer buffer);
	public float getNearPlane();
	public float getFarPlane();
	public void setNearFarPlanes(float near, float far);
	public void upload(FloatBuffer matrix44Buffer, int location);
	
}
