package math.matrix;

public class OrthogonalMatrix extends Matrix4f implements ProjectionMatrix {
	
	private float nearPlane;
	private float farPlane;
	
	public OrthogonalMatrix(float left, float right, float top, float bottom, float nearPlane, float farPlane){
		super();
		setLeftRightPlanes(left, right);
		setTopBottomPlanes(top, bottom);
		setNearFarPlanes(nearPlane, farPlane);
	}

	@Override
	public float getNearPlane() {
		return nearPlane;
	}

	@Override
	public float getFarPlane() {
		return farPlane;
	}
	
	public void setLeftRightPlanes(float left, float right) {
		float width = right - left;
		m00 = 2.0f / width;
		m30 = -(right + left)/width;
	}
	
	public void setTopBottomPlanes(float top, float bottom) {
		float height = top - bottom;
		m11 = 2.0f / height;
		m31 = -(top + bottom)/height;
	}

	@Override
	public void setNearFarPlanes(float near, float far) {
		float length = far - near;
		m22 = -2.0f / length;
		m32 = -(far + near)/length;
		nearPlane = near;
		farPlane = far;
	}

}
