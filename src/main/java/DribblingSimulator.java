/*
 * 3D Dribbling Simulator
 * University of Tartu, Computer Graphics Projects, Fall 2016
 * Authors: Anton T�ugunov, Richardas Ker�is
 */
import game.window.GameWindow;
import graphics.window.Window;
import graphics.window.WindowHandler;

public class DribblingSimulator {

	public static void main(String[] args) throws Exception {
		System.out.println("3D Dribbling Simulator");
		WindowHandler.init();
		Window gameWindow = new GameWindow();
		new Thread(gameWindow).start();
	}
	
}
