### Compilation/Launching Guide: ###
You need to have Java installed on your computer (Java 8 is recommended).  
Double-click the **run.bat** batch file to compile/run the code.  
### Controls ###
Use mouse to rotate the camera and hold left mouse button to add velocity to the ball.